require("dotenv").config()

/* pake puppeteer-extra */

const fssync = require("fs")
const like_post_service = require("./lib/like.js")
const comment_post_service = require("./lib/comment.js")
const report_post_service = require("./lib/report.js")
const login_service = require("./lib/login.js")
const share_post_service = require("./lib/share.js")
const cookies_service = require("./lib/cookies.js")

const config = {

    /* Facebook URLs that lead directly to post. */
    url_post: "https://www.facebook.com/187166408592742/posts/pfbid0nJcvCscfoE7R3YbwqA9QVFUMLVB3yrHBja7UkXrAG1ZgESDAqDox9YmwtmcJdECCl/?d=n",

    /* Change to `true` to like posts */
    like_post: true,

    /* Change to `true` to comment posts */
    comment_post: true,

    /* List of comment messages to be randomly selected */
    comment_msg: [
        "waduh kok bisa gan",
    ],

    /* Change to `true` to report posts */
    report_post: false,

    /* keywords to report.
     * options list 'nudity', 'spam', 'violence', 'unauthorized_sales', 'terrorism',
     *              'hate_speech', 'false_news', 'suicide', 'harassment'
     *
     * Options will be randomly selected if no keywords match
    */
    report_key: ".",

    /* Change to `true` to share posts */
    share_post: true,

    /* Post description */
    caption_share_post: "artikel bagus nih izin share gann",

    /* Credentials */
    email: process.env.EMAIL,
    password: process.env.PASSWORD,

    /* browser
    kalau ini di TRUE , maka akan pake puppeter ori yang udah include chromonium sehingga path yang dibawah
    akan diabaikan, */
    use_vanilla_puppeteer: true,

    /* chromium executable path */
    executable_path: "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe"
}


/*
 * SETUP BROWSER
 */

let puppeteer;
if (config.use_vanilla_puppeteer) {
    /* pake vanillaPuppeteer */
    const vanillaPuppeteer = require('puppeteer')
    const {addExtra} = require('puppeteer-extra')
    puppeteer = addExtra(vanillaPuppeteer)
} else {
    puppeteer = require('puppeteer-extra')
}
const StealthPlugin = require('puppeteer-extra-plugin-stealth')
puppeteer.use(StealthPlugin())

const AdblockerPlugin = require('puppeteer-extra-plugin-adblocker')
puppeteer.use(AdblockerPlugin({blockTrackers: true}))

const accounts = fssync.readdirSync("./accounts").filter(x => x.endsWith(".json"))

/* Validate user config */

try {
    new URL(config.url_post)
} catch (e) {
    console.log("[!] The url you provided is not valid")
    return
}

if (accounts.length == 0 && !process.argv.slice(2)[0]) {
    console.log("[!] Cannot find cookies file in ./accounts folder")
    return
}

if (process.argv.slice(2)[0] == "login") {
    let filename = `./accounts/${config.email}.json`
    if (!config.email || !config.password) {
        console.log("[!] It looks like the credentials you provided are not correct.")
        return
    } else if (fssync.existsSync(filename)) {
        console.log(`[!] File ${filename} already exists, please delete or rename it if you want to add a new account.`)
        return
    }
}

if (config.comment_post && config.comment_msg.length == 0) {
    console.log("[!] The comment message is empty, please edit the index.js file in the config section.")
    return
}

/* Run the browser in headless mode */
puppeteer.launch({
    headless: true,
    executablePath: !config.use_vanilla_puppeteer ? config.executable_path : null,
    args: [
        "--no-sandbox",
        "--disable-gpu",
    ]
}).then(async browser => {
    const page = await browser.newPage()
    await page.setViewport({width: 600, height: 800})
    console.log("[+] Bowser is ready.")

    if (process.argv.slice(2)[0] == "login") {
        if (await login_service(page, config.email, config.password)) {
            const filename = `accounts/${config.email}.json`
            await cookies_service.save(page, filename)
            console.log(`[+] Login successful, cookies saved in ${filename}.`)
        } else {
            console.log("[+] Login failed, please check your username and password again.")
        }
    } else {

        for (var i = 0; i < accounts.length; i++) {
            const filename = `accounts/${accounts[i]}`
            console.log(`[+] Processing: ${filename}`)

            await cookies_service.load(page, filename)

            console.log("    -> Open url.")
            await page.goto(config.url_post.replace(/\/[^f]+/, "//mbasic."), {waitUntil: "networkidle0"})

            let page_not_exists = await page.$$("xpath/.//*[@href[contains(., 'home.php') and contains(., 'rand')]]")
            if (page_not_exists[0]) {
                await cookies_service.save(page, filename)
                console.log("[!] The page you requested cannot be displayed right now.\n    It may be temporarily unavailable, the link you clicked on may be broken or expired,\n    or you may not have permission to view this page.")
                break
            }

            if (config.like_post) {
                await like_post_service(page)
                await page.waitForTimeout(2000)
            }

            if (config.comment_post) {
                let index = Math.floor(Math.random() * config.comment_msg.length);
                let comment = config.comment_msg[index];
                await comment_post_service(page, comment)
                await page.waitForTimeout(2000)
            }

            if (config.report_post) {
                await report_post_service(page, config.report_key)
                await page.waitForTimeout(2000)
            }

            if (config.share_post) {
                await share_post_service(page, config.caption_share_post)
                await page.waitForTimeout(2000)
            }

            await cookies_service.save(page, filename)
        }
    }
    console.log(`[+] Done. ✨`)
    await browser.close()
})
