const fs = require("fs/promises")

/* Function to load cookies. Valid cookies can be obtained after `login`
 * @params page  page instance
 *         path  file location
 */
const load = async (page, path) => {
    try {
        const raw = await fs.readFile(path, {encoding: "utf-8"})
        await page.setCookie(...JSON.parse(raw))
        console.log("    -> Cookies loaded.")
    } catch (error) {
        console.error(error)
    }
}

/* Function to save cookies into a file
 * @params page  page instance
 *         path  file location
 */
const save = async (page, path) => {
    const sessionData = await page.cookies()
    await fs.writeFile(path, JSON.stringify(sessionData, null, 2))
    console.log("    -> Cookies saved.")
}

module.exports = {
    load,
    save
}


