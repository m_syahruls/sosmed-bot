/* Function to share posts
 * @param page     page instance
 *        caption  text description
 */


const SHARE = "xpath/.//*[@id[contains(., 'actions_')]]//a[@href[contains(., 'share')]]"
const CAPTION = "xpath/.//*[@name='xc_message']"
const POST = "xpath/.//*[@name='view_post']"

module.exports = async (page, caption) => {
    console.log("    -> Trying to share a post.")
    await page.waitForTimeout(2000)

    try {
        /* Click then wait 5 seconds*/
        console.log(`       -> Click share button`)
        await page.click(SHARE)
        await page.waitForTimeout(5000)

        if (caption && caption.length > 0) {
            /* Add a caption and wait 2 seconds */
            console.log(`       -> Caption: ${caption}`)
            await page.type(CAPTION, caption)
            await page.waitForTimeout(2000)
        }

        /* Click the wait 5 seconds*/
        await page.click(POST)
        await page.waitForTimeout(5000)

        console.log("    -> Share successfully.")
        console.log(`       -> Link post: ${page.url().split('&eav')[0]}`)
        return true
    } catch (err) {
        console.error(`    -> Error: ${err.message}`)
    }

    /* selain itu gagal */
    console.log("    -> Failed to share.")
    return false
}



