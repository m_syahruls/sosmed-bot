
/* Function to like posts
 * @params page  page instance
 */

const LIKE_BUTTON = "xpath/.//*[@id[contains(., 'actions_')]]//a[@href[contains(., 'like') or contains(., 'react')]]"
const DELETE_LIKE = "xpath/.//a[@href[contains(., 'reaction_type=0')]]"
const CLOSE = "xpath/.//a[@href[contains(., '&id=')]]"

module.exports = async (page) => {
    console.log("    -> Trying to like a post.")
    await page.waitForTimeout(3000)
    try {
        /* Search for the like button */
        const like = await page.$$(LIKE_BUTTON)
        if (like[0]) {
            /* Click and pause for 3 seconds */
            console.log("       -> Click like button.")
            await like[0].click()
            await page.waitForTimeout(3000)

            let is_liked = 1
            const hapus = await page.$$(DELETE_LIKE)
            if (hapus[0]) {
                /* Click and pause for 3 seconds */
                console.log("       -> Unliking.")
                await hapus[0].click()
                await page.waitForTimeout(3000)
                is_liked = 2
            }

            if (page.url().indexOf("react") >= 0) {
                const tutupMenu = await page.$$(CLOSE)
                if (tutupMenu[0]) {
                    /* Click and pause for 3 seconds */
                    console.log("       -> Close pop up.")
                    await tutupMenu[0].click()
                    await page.waitForTimeout(3000)
                }
            }

            if (is_liked == 1) {
                console.log("    -> Successfully like a post.")
            } else {
                console.log("    -> Successfully unliked a post.")
            }
            return
        }
    } catch (err) {
        console.error(`    -> Error: ${err.message}`)
    }

    await page.waitForTimeout(2000)
    console.log("    -> Failed to like a post.")
}
