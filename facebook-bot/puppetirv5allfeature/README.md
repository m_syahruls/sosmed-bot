# fb_bot
facebook bot for like, comment, share and report

# .env
```
EMAIL = username/email
PASSWORD = password
```

You can login using the command `node index.js login`

# config didalam index.js
```javascript
{
    /* Facebook URLs that lead directly to post. */
    url_post: "https://www.facebook.com/100002418763588/posts/6026729357417630/?app=fbl",

    /* Change to `true` to like posts */
    like_post: true,

    /* Change to `true` to comment posts */
    comment_post: true,

    /* List of comment messages to be randomly selected */
    comment_msg: [
        ".",
    ],

    /* Change to `true` to report posts */
    report_post: true,

    /* keywords to report.
     * options list 'nudity', 'spam', 'violence', 'unauthorized_sales', 'terrorism',
     *              'hate_speech', 'false_news', 'suicide', 'harassment'
     *
     * Options will be randomly selected if no keywords match
    */
    report_key: "",

    /* Change to `true` to share posts */
    share_post: true,

    /* Post description */
    caption_share_post: "wkwkwk",

    /* Credentials */
    email: process.env.EMAIL,
    password: process.env.PASSWORD

    /* browser */
    use_vanilla_puppeteer: false,

    /* chromium executable path */
    executable_path: "/usr/lib/chromium/chrome"
}
```
