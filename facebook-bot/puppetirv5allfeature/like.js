
/* Function to like posts
 * @params page  page instance
 */

const LIKE_BUTTON = "xpath/.//*[@id[contains(., 'actions_')]]//a[@href[contains(., 'like')]]"
const UNLIKE_BUTTON = "xpath/.//*[@id[contains(., 'actions_')]]//a[@href[contains(., 'react')]]"
const DELETE_LIKE = "xpath/.//a[@href[contains(., 'reaction_type=0')]]"
const CLOSE = "xpath/.//a[@href[contains(., '&id=')]]"

module.exports = async (page, like_post, unlike_post) => {
    console.log(`    -> Trying to ${like_post ? 'like' : 'unlike'} a post.`)
    await page.waitForTimeout(3000)
    try {

        const like_button = await page.$$(LIKE_BUTTON)
        const unlike_button = await page.$$(UNLIKE_BUTTON)

        let is_ok = false
        if (like_post) {
            if (unlike_button[0] && !like_button[0]) {
                console.log("    -> Skip this post has been liked before.")
                return
            } else if (like_button[0]) {
                console.log("       -> Click like button.")
                await like_button[0].click()
                await page.waitForTimeout(3000)

                is_ok = true
            }
        } else if (unlike_post) {

            /* If both buttons are available,
             * it means that the post has never been liked before
             */

            if (unlike_button[0] && like_button[0]) {
                console.log("    -> Skip this post has never been liked before.")
                return
            } else if (unlike_button[0]) {
                console.log("       -> Click react button.")
                await unlike_button[0].click()
                await page.waitForTimeout(3000)

                console.log("       -> Delete previous like.")
                await page.click(DELETE_LIKE)
                await page.waitForTimeout(3000)

                is_ok = true
            }
        }

        if (page.url().indexOf("react") >= 0) {
            const tutup_menu = await page.$$(CLOSE)
            if (tutup_menu[0]) {
                console.log("       -> Close pop up.")
            }
        }

        if (is_ok) {
            if (like_post) {
                console.log("    -> Like post successfully.")
            } else if (unlike_post) {
                console.log("    -> Unlike post successfully.")
            }
            return
        }
    } catch (err) {
        console.error(`    -> Error: ${err.message}`)
    }

    await page.waitForTimeout(2000)
    console.log(`    -> ${like_post ? 'Like' : 'Unlike'} post failed.`)
}
