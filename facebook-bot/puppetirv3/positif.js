const puppeteer = require("puppeteer-extra");
const StealthPlugin = require("puppeteer-extra-plugin-stealth");
const dotenv = require("dotenv");
const cookieHelper = require("../../helper/cookie");
const { xpathList } = require("./constant");
const { loginServices } = require("./services");
puppeteer.use(StealthPlugin());
dotenv.config();

const args = process.argv.slice(2)[0];
(async () => {
  const browser = await puppeteer.launch({
    headless: false,
    executablePath:
      "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe",
    args: [
      "--no-sandbox",
      "--disable-gpu",
      "--enable-webgl",
      // "--start-maximized",
      "--window-size=1920,1080",
    ],
  });
  const page = await browser.newPage();
  let comments;
  const url =
    "https://www.facebook.com/187166408592742/posts/pfbid02XB1GDJkJUHaevPJ2ZCand56kn519TetkbLEiHtyUKr1WMV6ewyQAWHSyknsHpZqPl/?d=n";

  //Checking if there a cookie
  const isAuth = await cookieHelper.checkCookie(page);
  if (!isAuth) {
    await loginServices.login(page);
    await cookieHelper.saveCookie(page);
  }
  await cookieHelper.storeCookie(page);
  await page.goto(url);
  // await page.goto(url.replace(/\/[^f]+/, "//m."), {
  //   waitUntil: "networkidle0",
  // });

  // const isLike = async (page) => {
  //   await page.waitForSelector(xpathList.likeDisplay);
  //   const likeButton = await page.$(xpathList.likeDisplay);
  //   const isDisplayed = await page.evaluate(xpathList.likeDisplay, likeButton);
  //   // const isDisplayed = await By.class("x1b0d499 xi3auck", likeButton);
  //   return isDisplayed;
  // };
  // const isNotLiked = async (page) => {
  //   await page.waitForSelector(xpathList.like);
  //   const likeButton = await page.$(xpathList.like);
  //   const isDisplayed = await page.evaluate(xpathList.like, likeButton);
  //   // const isDisplayed = await By.class("x1b0d499 x1d69dk1", likeButton);
  //   return isDisplayed;
  // };

  // const isLiked = await isLike(page);
  // const isNotLikedd = await isNotLiked(page);
  // console.log(isNotLikedd);

  try {
    console.log(isLiked);
    await page.waitForTimeout(1000);
    await page.waitForSelector(xpathList.like);
    await page.click(xpathList.like);
    await page.waitForTimeout(1000);
    await page.waitForSelector(xpathList.comment);
    await page.click(xpathList.comment);
    await page.type(xpathList.comment, "sangat bermanfaat");
    await page.waitForTimeout(2000);
    await page.click(xpathList.submitComment);
    console.log("Like and comment successfull");
    // await browser.close();
  } catch (error) {
    console.log("Like and Comment unsuccesfull" + error);
  }
})();
