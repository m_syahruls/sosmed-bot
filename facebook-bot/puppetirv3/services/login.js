const puppeteer = require("puppeteer-extra");
const { xpathList } = require("../constant");
const login_fb = "https://www.facebook.com/login.php";
const url =
  "https://www.facebook.com/187166408592742/posts/pfbid02XB1GDJkJUHaevPJ2ZCand56kn519TetkbLEiHtyUKr1WMV6ewyQAWHSyknsHpZqPl/?d=n";
const EMAIL_SELECTOR = "#email";
const PASSWORD_SELECTOR = "#pass";

//fill login dan sumbit

const login = async (page) => {
  return new Promise(async (resolve, reject) => {
    try {
      await page.goto(login_fb, { waitUntil: "networkidle2" });
      await page.waitForTimeout(3000);
      await page.type(EMAIL_SELECTOR, process.env.EMAIL);
      await page.type(PASSWORD_SELECTOR, process.env.PASSWORD);
      await page.waitForTimeout(3000);
      await page.click(xpathList.clickLogin);
      await page.waitForTimeout(3000);
      resolve("login: successfully");
    } catch (error) {
      reject("login error: " + error);
    }
    await page.waitForTimeout(1000);
  });
};
module.exports = {
  login,
};
