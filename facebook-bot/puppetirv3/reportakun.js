const puppeteer = require("puppeteer-extra");
const StealthPlugin = require("puppeteer-extra-plugin-stealth");
const dotenv = require("dotenv");
const cookieHelper = require("../../helper/cookie");
const { xpathList } = require("./constant");
puppeteer.use(StealthPlugin());
dotenv.config();

const args = process.argv.slice(2)[0];
(async () => {
  const browser = await puppeteer.launch({
    headless: false,
    executablePath:
      "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe",
    args: [
      "--no-sandbox",
      "--disable-gpu",
      "--enable-webgl",
      // "--start-maximized",
      "--window-size=1920,1080",
    ],
  });
  const page = await browser.newPage();
  let comments;
  const url = "https://www.facebook.com/terebishitposting/";

  //Checking if there a cookie
  const isAuth = await cookieHelper.checkCookie(page);
  if (!isAuth) {
    await loginServices.login(page);
    await cookieHelper.saveCookie(page);
  }
  await cookieHelper.storeCookie(page);
  await page.goto(url);
  // await page.goto(url.replace(/\/[^f]+/, "//m."), {
  //   waitUntil: "networkidle0",
  // });

  try {
    await page.waitForSelector(xpathList.moreButtonAkun);
    await page.click(xpathList.moreButtonAkun);
    await page.waitForTimeout(2000);
    await page.waitForSelector(xpathList.clickReportAkun);
    await page.click(xpathList.clickReportAkun);
    await page.waitForSelector(xpathList.clickFakeInformation);
    await page.click(xpathList.clickFakeInformation);
    await page.waitForSelector(xpathList.clickPolitik);
    await page.click(xpathList.clickPolitik);
    await page.waitForSelector(xpathList.submitReportAkun);
    await page.click(xpathList.submitReportAkun);
    console.log("Report successfull");
    await page.waitForTimeout(1000);
    await browser.close();
  } catch (error) {
    console.log("Report unsuccessfull" + error);
  }
})();
