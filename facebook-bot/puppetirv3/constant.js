const xpathList = {
  like: "xpath//html/body/div[1]/div/div[1]/div/div[5]/div/div/div[3]/div/div/div[1]/div[1]/div[4]/div[1]/div/div/div/div/div/div/div/div/div/div[1]/div/div[2]/div/div/div[4]/div/div/div[1]/div/div[2]/div/div[1]",
  clickLogin:
    "xpath//html/body/div[1]/div[1]/div[1]/div/div[2]/div[2]/form/div/div[3]/button",
  comment:
    "xpath//html/body/div[1]/div/div[1]/div/div[5]/div/div/div[3]/div/div/div[1]/div[1]/div[4]/div[1]/div/div/div/div/div/div/div/div/div/div[1]/div/div[2]/div/div/div[4]/div/div/div[2]/div[3]/div/div[2]/div[1]/form/div/div/div[1]/div",
  submitComment:
    "xpath//html/body/div[1]/div/div[1]/div/div[5]/div/div/div[3]/div/div/div[1]/div[1]/div[4]/div[1]/div/div/div/div/div/div/div/div/div/div[1]/div/div[2]/div/div/div[4]/div/div/div[2]/div[3]/div/div[2]/div[1]/form/div/div[2]/div/div[2]/div/div/div",
  moreButton:
    "xpath//html/body/div[1]/div/div[1]/div/div[5]/div/div/div[3]/div/div/div[1]/div[1]/div[4]/div[1]/div/div/div/div/div/div/div/div/div/div[1]/div/div[2]/div/div/div[2]/div/div[3]/div/div",
  clickReport:
    "xpath//html/body/div[1]/div/div[1]/div/div[5]/div/div/div[3]/div/div/div[2]/div/div/div[1]/div[1]/div/div/div/div/div/div/div[1]/div/div[5]",
  clickHate:
    "xpath//html/body/div[1]/div/div[1]/div/div[6]/div/div/div[2]/div/div/div[1]/div/div[2]/div/div/div/div[3]/div/div[2]/div/div/div[7]/div/div/div/div[1]/div/div[1]/div/div/div/div",
  clickSociety:
    "xpath//html/body/div[1]/div/div[1]/div/div[6]/div/div/div[2]/div/div/div[1]/div/div[2]/div/div/div/div[4]/div/div[2]/div/div/div[3]/div/div/div/div[1]/div/div[1]/div/div/div/div",
  submitReport:
    "xpath//html/body/div[1]/div/div[1]/div/div[6]/div/div/div[2]/div/div/div[1]/div/div[2]/div/div/div/div[4]/div/div/div[4]",
  moreButtonAkun:
    "xpath//html/body/div[1]/div/div[1]/div/div[5]/div/div/div[3]/div/div/div[1]/div[1]/div[3]/div/div/div/div[2]/div/div/div[4]/div/div",
  clickReportAkun:
    "xpath//html/body/div[1]/div/div[1]/div/div[5]/div/div/div[3]/div/div/div[2]/div/div/div[1]/div[1]/div/div/div/div/div/div/div[1]/div/div[6]",
  clickFakeInformation:
    "xpath//html/body/div[1]/div/div[1]/div/div[6]/div/div/div[2]/div/div/div[1]/div/div[2]/div/div/div/div[3]/div/div[2]/div/div/div[4]/div/div/div/div[1]/div",
  clickPolitik:
    "xpath/html/body/div[1]/div/div[1]/div/div[6]/div/div/div[2]/div/div/div[1]/div/div[2]/div/div/div/div[4]/div/div[2]/div/div/div[2]/div/div/div/div[1]/div",
  submitReportAkun:
    "xpath//html/body/div[1]/div/div[1]/div/div[6]/div/div/div[2]/div/div/div[1]/div/div[2]/div/div/div/div[4]/div/div/div[4]/div",
  likeDisplay:
    "xpath//html/body/div[1]/div/div[1]/div/div[5]/div/div/div[3]/div/div/div[1]/div[1]/div/div/div/div/div/div/div/div/div/div/div/div/div/div[2]/div/div/div[4]/div/div/div[1]/div/div[2]/div/div[1]/div[1]/div[1]/div[1]/span",
};

module.exports.xpathList = xpathList;
