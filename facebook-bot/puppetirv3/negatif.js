const puppeteer = require("puppeteer-extra");
const StealthPlugin = require("puppeteer-extra-plugin-stealth");
const dotenv = require("dotenv");
const cookieHelper = require("../../helper/cookie");
const { xpathList } = require("./constant");
const { loginServices } = require("./services");
puppeteer.use(StealthPlugin());
dotenv.config();

const args = process.argv.slice(2)[0];
(async () => {
  const browser = await puppeteer.launch({
    headless: false,
    executablePath:
      "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe",
    args: [
      "--no-sandbox",
      "--disable-gpu",
      "--enable-webgl",
      // "--start-maximized",
      "--window-size=1920,1080",
    ],
  });
  const page = await browser.newPage();
  let comments;
  const url =
    "https://www.facebook.com/187166408592742/posts/pfbid02XB1GDJkJUHaevPJ2ZCand56kn519TetkbLEiHtyUKr1WMV6ewyQAWHSyknsHpZqPl/?d=n";

  //Checking if there a cookie
  const isAuth = await cookieHelper.checkCookie(page);
  if (!isAuth) {
    await loginServices.login(page);
    await cookieHelper.saveCookie(page);
  }
  await cookieHelper.storeCookie(page);
  await page.goto(url);
  // await page.goto(url.replace(/\/[^f]+/, "//m."), {
  //   waitUntil: "networkidle0",
  // });

  //process comment
  try {
    await page.waitForSelector(xpathList.comment);
    await page.click(xpathList.comment);
    await page.type(xpathList.comment, "gabener ni konten nya");
    await page.click(xpathList.submitComment);
    await page.waitForTimeout(1000);
  } catch (error) {
    console.log("Comment unsuccessfull" + error);
  }

  //Process report
  try {
    await page.waitForSelector(xpathList.moreButton);
    await page.click(xpathList.moreButton);
    await page.waitForTimeout(2000);
    await page.waitForSelector(xpathList.clickReport);
    await page.click(xpathList.clickReport);
    await page.waitForSelector(xpathList.clickHate);
    await page.click(xpathList.clickHate);
    await page.waitForSelector(xpathList.clickSociety);
    await page.click(xpathList.clickSociety);
    await page.waitForSelector(xpathList.submitReport);
    await page.click(xpathList.submitReport);
    await page.waitForTimeout(1000);
  } catch (error) {
    console.log("Report unsuccessfull" + error);
  }
})();
