require("dotenv").config();
const puppeteer = require("puppeteer-extra");
const fs = require("fs/promises");
const fssync = require("fs");

const StealthPlugin = require("puppeteer-extra-plugin-stealth");
puppeteer.use(StealthPlugin());

const AdblockerPlugin = require("puppeteer-extra-plugin-adblocker");
puppeteer.use(AdblockerPlugin({ blockTrackers: true }));

const likePostFunc = require("./lib/like.js");
const commentPostFunc = require("./lib/comments.js");
const reportPostFunc = require("./lib/report.js");
const loginFunc = require("./lib/login.js");

const config = {
  /* url facebook yang mengarah langsung ke postingan
   * bisa ditandai dengan kata 'post' didalam url */
  url_post:
    "https://www.facebook.com/photo/?fbid=3997813283777764&set=a.1610208972538219",

  /* ubah ke true untuk melakukan action like post */
  like_post: true,

  /* ubah ke true untuk melakukan action comment post */
  comment_post: true,

  /* daftar komentar yang akan dipilih random */
  comment_msg: ["wkwkwk", "hahaha", "tes komen"],

  /* ubah ke true untuk melakukan action report post */
  report_post: true,

  /* kata kunci untuk melakukan report, contoh spam, informasi palsu, ketelanjangan
   * jika kata kunci tidak tersedia maka akan otomatis dipilih secara acak */
  report_key: "tela",

  /* kredensial */
  email: process.env.EMAIL,
  password: process.env.PASSWORD,
};

/* fungsi untuk memuat cookie dari file json
 * cookies harus berisi 3 key utama yaitu
 *   1. name
 *   2. value
 *   3. domain
 */
const loadCookie = async (page, path) => {
  try {
    const raw = await fs.readFile(path, { encoding: "utf-8" });
    await page.setCookie(...JSON.parse(raw));
    console.log("  Cookies dimuat.");
  } catch (error) {
    console.error(error);
  }
};

/* fungsi untuk menyimpan cookies kedalam file
 * karena cookies akan selalu berubah maka
 * sebaiknya panggil fungsi ini setiap melakukan
 * `action` ke page, contoh membuka url baru, click, dll
 */
const saveCookie = async (page, path) => {
  const sessionData = await page.cookies();
  await fs.writeFile(path, JSON.stringify(sessionData, null, 2));
  console.log("  Cookies disimpan.");
};

const accounts = fssync
  .readdirSync("./accounts")
  .filter((x) => x.endsWith(".json"));

if (accounts.length == 0 && !process.argv.slice(2)[0]) {
  console.log("Oppss, tidak dapat menemukan cookies di folder ./accounts/");
  return;
}

if (config.comment_post && config.comment_msg.length == 0) {
  console.log(
    "Oppss, Pesan komentar kosong, silahkan edit file index.js dibagian config untuk menambahkan pesan."
  );
} else {
  /* jalankan browser secara headless */
  puppeteer
    .launch({
      headless: false,
      executablePath:
        "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe",
      args: ["--no-sandbox", "--disable-gpu"],
    })
    .then(async (browser) => {
      const page = await browser.newPage();
      await page.setViewport({ width: 600, height: 800 });

      if (process.argv.slice(2)[0] == "login") {
        let filename = `./accounts/${config.email}.json`;
        if (!config.email || !config.password) {
          console.log(
            "Oopss, sepertinya kamu belum memberikan kredensial dengan benar."
          );
        } else if (fssync.existsSync(filename)) {
          console.log(
            `Oopss, sepertinya file ${filename} sudah ada, silahkan hapus jika ingin login ulang`
          );
        } else if (await loginFunc(page, config.email, config.password)) {
          await saveCookie(page, filename);
          console.log(
            `Login berhasil. cookies tersimpan di folder ${filename}`
          );
        } else {
          console.log(
            "Login gagal, silahkan cek ulang kembali email atau password."
          );
        }
      } else {
        for (var i = 0; i < accounts.length; i++) {
          const filename = `accounts/${accounts[i]}`;
          console.log(`Muat cookies: ${filename}`);

          await loadCookie(page, filename);
          console.log("  Membuka link postingan.");
          await page.goto(config.url_post.replace(/\/[^f]+/, "//mbasic."), {
            waitUntil: "networkidle0",
          });

          if (config.like_post) {
            let liked = await likePostFunc(page);

            if (liked == 1) {
              console.log("  Postingan berhasil disukai.");
            } else if (liked == 2) {
              console.log("  Tidak lagi menyukai postingan.");
            } else {
              console.log("  Postingan gagal disukai.");
            }
          }

          if (config.comment_post) {
            let index = Math.floor(Math.random() * config.comment_msg.length);
            let comment = config.comment_msg[index];

            if (await commentPostFunc(page, comment)) {
              console.log("  Postingan berhasil dikomentari.");
            } else {
              console.log("  Postingan gagal dikomentari.");
            }
          }

          if (config.report_post) {
            if (await reportPostFunc(page, config.report_key)) {
              console.log("  Postingan berhasil dilaporkan.");
            } else {
              console.log("  Postingan gagal dilaporkan.");
            }
          }

          await saveCookie(page, filename);
        }
      }
      console.log(`Selesai. ✨`);
      await browser.close();
    });
}
