const {default: JSSoup} = require("jssoup")

/* fungsi untuk melaporkan postingan
 * param @page  page instance
 *       @rKey  laporkan atas dasar
 *              - eg. informasi palsu
 */
module.exports = async (page, rKey) => {
    console.log("  Melaporkan postingan.")
    try {

        /* click Lainnya */
        const lainnya = await page.waitForXPath(".//*[text()[contains(., 'Lainnya')]]")
        console.log("    -> Lainnya")
        await lainnya.click()
        await page.waitForTimeout(2000)

        /* click laporkan */
        const laporkan = await page.waitForXPath(".//*[text()[contains(., 'laporkan')]]")
        console.log("    -> Minta bantuan atau laporkan postingan")
        await laporkan.click()
        await page.waitForTimeout(2000)

        /* click Kirim */
        const kirim = await page.waitForXPath(".//input[@value='Kirim']")
        console.log("    -> Kirim")
        await kirim.click()
        await page.waitForTimeout(2000)

        /* loop untuk mencari label laporan yang sesuai
         * jika tidak ada pilih acak
         * ulangi terus sampai pilihan habis
         */
        let first = true;
        while (true) {
            const soup = new JSSoup(await page.content())
            let action_keys = soup.findAll("input", {name: "tag"})
            let labels = []
            let action_index = null

            let i = 0;
            for (action of action_keys) {
                const label = action.previousElement.previousElement.find("span").text.trim();
                labels.push(label)

                if (first && label.toLowerCase().indexOf(rKey.toLowerCase()) >= 0) {
                    action_index = i
                    first = false
                }
                i++
            }
            if (Object.keys(labels).length == 0) break
            if (!action_index) action_index = Math.floor(Math.random() * Object.keys(labels).length)
            /* buat xpath */
            console.log(`    -> Laporkan: ${labels[action_index]}`)
            const attrs = action_keys[action_index].attrs
            const xpath = ".//input[@value='" + attrs.value + "']"

            /* click `label` */
            const select = await page.waitForXPath(xpath)
            await select.click()
            await page.waitForTimeout(2000)

            /* click kirim */
            const kirim = await page.waitForXPath(".//input[@value='Kirim']")
            console.log("    -> Kirim")
            await kirim.click()
            await page.waitForTimeout(2000)
        }

        /* click Checkbox */
        const checkbox = await page.waitForXPath(".//*[@id='m_check_list_aria_label']/input")
        console.log("    -> Checkbox")
        await checkbox.click()
        await page.waitForTimeout(2000)

        /* click Laporkan lalu Kirim */
        for (val of ["Laporkan", "Kirim"]) {
            const kirimLaporan = await page.waitForXPath(".//input[@value='" + val + "']")
            console.log("    -> " + val)
            await kirimLaporan.click()
            await page.waitForTimeout(2000)
        }

        /* click Selesai */
        const selesai = await page.waitForXPath(".//*[text()[contains(., 'Selesai')]]")
        console.log("    -> Selesai")
        await selesai.click()
        await page.waitForTimeout(2000)

        /* kembalikan nilai true */
        return true
    } catch (err) {
        console.error(`  ${err}`)
    }
    /* selain itu false */
    return false
}


