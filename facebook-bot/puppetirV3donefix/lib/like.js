
/* fungsi untuk Like postingan
 * param @page  page instance
 *
 * Note: pastikan settingan bahasa adalah
 *       bahasa indonesia untuk menghindari error
 *
 */

module.exports = async (page) => {
    console.log("  Menyukai postingan.")
    try {
        /* cari tombol suka */
        const like = await page.$$("xpath/.//*[text()[contains(., 'Suka')]]")
        if (like[0]) {
            /* click lalu beri jeda 3 detik */
            console.log("    -> Suka")
            await like[0].click()
            await page.waitForTimeout(3000)

            let isLiked = 1
            const hapus = await page.$$("xpath/.//*[text()[contains(., '(Hapus)')]]")
            if (hapus[0]) {
                /* click untuk menghapus like lalu jeda 3 detik */
                console.log("    -> Hapus/Unlike")
                await hapus[0].click()
                await page.waitForTimeout(3000)
                isLiked = 2
            }

            const tutupMenu = await page.$$("xpath/.//*[text()[contains(., 'Tutup')]]")
            if (tutupMenu[0]) {
                /* click untuk menutup menu lalu jeda 3 detik*/
                console.log("    -> Tutup")
                await tutupMenu[0].click()
                await page.waitForTimeout(3000)
            }

            /* kembalikan nilai 1: like, 2: unlike */
            return isLiked
        }
    } catch (err) {
        console.error(`  ${err.message}`)
    }
    /* selain itu false / gagal */
    await page.waitForTimeout(2000)
    return false
}
