
/* fungsi untuk login
 * param @page      page instance
 *       @username
 *       @password
 */

module.exports = async (page, username, password) => {
    console.log("Mencoba login.")
    try {
        /* clear cookies */
        console.log("  Menghapus cookies.")
        const client = await page.target().createCDPSession()
        await client.send('Network.clearBrowserCookies')

        console.log("  Membuka halaman login.")
        await page.goto("https://mbasic.facebook.com/login.php")
        await page.waitForTimeout(5000)

        console.log("    -> Input username.")
        await page.type("xpath/.//*[@name='email']", username)
        console.log("    -> Input password.")
        await page.type("xpath/.//*[@name='pass']", password)
        console.log("    -> Submit.")
        await page.click("xpath/.//*[@id='login_form']/ul/li[3]/input")

        await page.waitForTimeout(10000)

        /* check login error, jika ditemukan berarti login gagal */
        let html = await page.content()
        return html.indexOf("login_error") < 0
    } catch (err) {
        console.error(`  ${err.message}`)
    }
    /* selain itu false / gagal */
    await page.waitForTimeout(2000)
    return false
}
