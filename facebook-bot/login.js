const { By, Builder } = require('selenium-webdriver');

require('dotenv').config();
require('chromedriver');
var chrome = require('selenium-webdriver/chrome');
var options = new chrome.Options();

const { email, pass, debuggerAddress } = {
  email: process.env.EMAIL,
  pass: process.env.PASS,
  debuggerAddress: process.env.DEBUGGER_ADDRESS,
};

// options.addArguments('--headless');
// options.addArguments('--no-gpu');

// options.addArguments(
//   'user-data-dir=/Users/faisal/Library/Application Support/Google/Chrome'
// );
// options.addArguments('profile-directory=Default');
// options.addArguments('--headless');
// options.addArguments('--no-sandbox');
options.addArguments('--disable-dev-shm-usage'); // overcome limited resource problems
options.addArguments('--disable-notifications');
// options.debuggerAddress(debuggerAddress); // existing browser

async function example() {
  var searchString = 'Automation testing with Selenium';

  //To wait for browser to build and launch properly

  //To fetch http://google.com from the browser with our code.

  try {
    let driver = await new Builder()
      .forBrowser('chrome')
      .setChromeOptions(options)
      .build();

    // to get debugger address
    // const capabilities = await driver.getCapabilities();
    // console.log(capabilities);

    await driver.get(
      'https://www.facebook.com/permalink.php?story_fbid=pfbid02tBT5ytQznPifAT7SU3oYaz4rfPU4izN7rxc7BeYsdnreHr3TFjPT5HAr9ynia3vWl&id=100020512705235'
    );

    const emailField = await driver
      .findElement(By.css('[name="email"]'))
      .sendKeys(email);
    const passField = await driver
      .findElement(By.css('[name="pass"]'))
      .sendKeys(pass);
    const buttonSubmit = await driver
      .findElement(By.css('[type="submit"]'))
      .click();

    // await driver.close();
  } catch (error) {
    // console.log('called', error);
    // await driver.close();
  }

  //To send a search query by passing the value in searchString.
  // await driver.findElement(By.name('q')).sendKeys(searchString, Key.RETURN);

  //Verify the page title and print it
  // var title = await driver.getTitle();
  // console.log('Title is:', title);

  //It is always a safe practice to quit the browser after execution
  // await driver.quit();
}

example();
