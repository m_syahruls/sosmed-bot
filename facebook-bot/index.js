const { By, Key, Builder, until } = require('selenium-webdriver');

var chrome = require('selenium-webdriver/chrome');

require('chromedriver');

var options = new chrome.Options();

// options.addArguments('--headless');
// options.addArguments('--no-gpu');

// options.addArguments(
//   'user-data-dir=/Users/faisal/Library/Application Support/Google/Chrome'
// );
// options.addArguments('profile-directory=Default');
// options.addArguments('--headless');
// options.addArguments('--no-sandbox');
options.addArguments('--disable-dev-shm-usage'); // overcome limited resource problems
options.addArguments('--disable-notifications');

async function example() {
  var searchString = 'Automation testing with Selenium';

  //To wait for browser to build and launch properly

  //To fetch http://google.com from the browser with our code.

  try {
    let driver = await new Builder()
      .forBrowser('chrome')
      .setChromeOptions(options)
      .build();

    await driver.get('https://www.facebook.com/login/');

    const emailField = await driver.findElement(By.css('[name="email"]'));
    emailField.sendKeys(email);
    const passField = await driver.findElement(By.css('[name="pass"]'));
    passField.sendKeys(pass);
    // const buttonSubmit = await driver.findElement(By.css('[type="submit"]'));
    // buttonSubmit.click();

    // await driver.close();
  } catch (error) {
    // console.log('called', error);
    // await driver.close();
  }

  //To send a search query by passing the value in searchString.
  // await driver.findElement(By.name('q')).sendKeys(searchString, Key.RETURN);

  //Verify the page title and print it
  // var title = await driver.getTitle();
  // console.log('Title is:', title);

  //It is always a safe practice to quit the browser after execution
  // await driver.quit();
}

example();
