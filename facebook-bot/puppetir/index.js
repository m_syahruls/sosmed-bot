const puppeteer = require('puppeteer-extra');
const fs = require('fs/promises');

const StealthPlugin = require('puppeteer-extra-plugin-stealth');
puppeteer.use(StealthPlugin());

const AdblockerPlugin = require('puppeteer-extra-plugin-adblocker');
puppeteer.use(
  AdblockerPlugin({
    blockTrackers: true,
  })
);

/* fungsi untuk memuat cookie dari file json
 * cookies harus berisi 3 key yaitu
 *   - name
 *   - value
 *   - domain
 */
const loadCookie = async (page, path) => {
  try {
    const raw = await fs.readFile(path, {
      encoding: 'utf-8',
    });
    await page.setCookie(...JSON.parse(raw));
    console.log('Cookies loaded');
  } catch (error) {
    console.error(error);
  }
};

/* fungsi untuk menyimpan cookies kedalam file
 * karena cookies akan selalu berubah maka
 * sebaiknya panggil fungsi ini setiap melakukan
 * `action` ke page, contoh membuka url baru, click, dll
 */
const saveCookie = async (page, path) => {
  const sessionData = await page.cookies();
  await fs.writeFile(path, JSON.stringify(sessionData, null, 2));
  console.log('Cookies updated');
};

/* fungsi untuk Like/Unlike postingan
 * Note: untuk fitur ini masih ada kendala yaitu
 *       postingan terkadang berhasil disukai namun
 *       info yang ditampilkan tidak sesuai */
const likePost = async (page) => {
  const like = await page.$$("xpath/.//*[text()[contains(., 'Suka')]]");
  if (like[0]) {
    await like[0].click();
    await page.waitForTimeout(2000);
    const unlike = await page.$$("xpath/.//*[text()[contains(., 'Hapus')]]");
    if (unlike[0]) {
      await unlike[0].click();
      console.log('Postingan tidak lagi disukai');
    } else {
      console.log('Postingan berhasil disukai');
    }
  }
};

/* fungsi untuk mengomentari postingan */
const commentPost = async (page, msg) => {
  const commentArea = await page.$('#composerInput');
  if (commentArea) {
    await commentArea.type(msg);
    await page.waitForTimeout(1000);

    const submit = await page.$$("xpath/.//input[@value='Komentari']");
    if (submit[0]) {
      await submit[0].click();
      console.log('Komentar berhasil ditambahkan');
    } else {
      console.log('Komentar tidak berhasil ditambahkan');
    }
  }
};

/* jalankan browser secara headless */
puppeteer
  .launch({
    headless: false,
    executablePath:
      '/Applications/Google Chrome.app/Contents/MacOS/Google Chrome',
    args: ['--no-sandbox', '--disable-gpu'],
  })
  .then(async (browser) => {
    let url =
      'https://m.facebook.com/story.php?story_fbid=pfbid0LXxehWxhz1oKPFMUzSBddWdwWEzECxVYzB1hks5tNm8BBLXTteYTt7FnhbuW9YXml&id=100004474623467&mibextid=Nif5oz';

    /* ganti subdomain menjadi mbasic */
    url = url.replace(/\/[^f]+/, '//mbasic.');

    const page = await browser.newPage();
    await page.setViewport({
      width: 600,
      height: 800,
    });
    await loadCookie(page, 'cookies.json');

    console.log('Membuka url: ' + url);
    await page.goto(url);
    await page.waitForTimeout(5000);

    try {
      await likePost(page);
      await page.waitForTimeout(4000);
      await commentPost(page, 'ini mantebss');
    } catch (error) {
      console.error(error);
    }

    await saveCookie(page, 'cookies.json');
    console.log(`All done. ✨`);
    await browser.close();
  });
