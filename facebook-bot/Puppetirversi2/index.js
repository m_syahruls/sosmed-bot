const puppeteer = require('puppeteer-extra')
const fs = require("fs/promises")
const fssync = require("fs")
const { default: JSSoup } = require("jssoup")
const prompt = require("prompt-sync")({ sigint: true });

const StealthPlugin = require('puppeteer-extra-plugin-stealth')
puppeteer.use(StealthPlugin())

const AdblockerPlugin = require('puppeteer-extra-plugin-adblocker')
puppeteer.use(AdblockerPlugin({ blockTrackers: true }))

const likePost = require("./lib/like.js")
const commentPost = require("./lib/comment.js")

/* fungsi untuk memuat cookie dari file json
 * cookies harus berisi 3 key utama yaitu
 *   1. name
 *   2. value
 *   3. domain
 */
const loadCookie = async(page, path) => {
    try {
        const raw = await fs.readFile(path, { encoding: "utf-8" })
        await page.setCookie(...JSON.parse(raw))
        console.log("Cookies dimuat.")
    } catch (error) {
        console.error(error)
    }
}

/* fungsi untuk menyimpan cookies kedalam file
 * karena cookies akan selalu berubah maka
 * sebaiknya panggil fungsi ini setiap melakukan
 * `action` ke page, contoh membuka url baru, click, dll
 */
const saveCookie = async(page, path) => {
    const sessionData = await page.cookies()
    await fs.writeFile(path, JSON.stringify(sessionData, null, 2))
    console.log("Cookies disimpan.")
}


/* fungsi untuk melaporkan postingan
 * Note: fungsi eksperimental
 *       mungkin output masih ada yang tidak sesuai
 */
const reportPost = async(page) => {
    console.log("Melaporkan postingan.")
    await page.waitForTimeout(2000)

    console.log("  -> Lainnya")
    const lainnya = await page.waitForXPath(".//*[text()[contains(., 'Lainnya')]]")
    await lainnya.click()
    await page.waitForTimeout(2000)

    console.log("  -> Minta bantuan atau laporkan postingan")
    const laporkan = await page.waitForXPath(".//*[text()[contains(., 'laporkan')]]")
    await laporkan.click()
    await page.waitForTimeout(2000)

    console.log("  -> Kirim")
    const kirim = await page.waitForXPath(".//input[@value='Kirim']")
    await kirim.click()
    await page.waitForTimeout(2000)

    let action_keys = null
    while (true) {
        const soup = new JSSoup(await page.content())
        action_keys = soup.findAll("input", { name: "tag" })
        let labels = []
        for (action of action_keys) {
            const label = action.previousElement.previousElement.find("span").text.trim();
            labels.push(label)
        }

        if (labels.length == 0) break

        for (let i = 0; i < Object.keys(labels).length; i++) {
            console.log("  [" + (i + 1) + "] " + labels[i])
        }

        let input;
        while (true) {
            input = null
            while (!input || !/^\d+/.test(input)) {
                input = prompt("  >> pilih (1-" + labels.length + "): ")
            }
            input = parseInt(input.trim()) - 1
            if (input >= 0 && input < labels.length) break
        }
        const attrs = action_keys[input].attrs
        const xpath = ".//input[@value='" + attrs.value + "']"

        console.log("  -> " + labels[input])
        const select = await page.waitForXPath(xpath)
        await select.click()
        await page.waitForTimeout(2000)

        console.log("  -> Kirim")
        const kirim = await page.waitForXPath(".//input[@value='Kirim']")
        await kirim.click()
        await page.waitForTimeout(2000)
    }

    console.log("  -> Checkbox")
    const checkbox = await page.waitForXPath(".//*[@id='m_check_list_aria_label']/input")
    await checkbox.click()
    await page.waitForTimeout(2000)

    for (val of["Laporkan", "Kirim"]) {
        console.log("  -> " + val)
        const kirimLaporan = await page.waitForXPath(".//input[@value='" + val + "']")
        await kirimLaporan.click()
        await page.waitForTimeout(2000)
    }

    await page.screenshot({ path: "laporan.png", fullPage: true })
    await page.waitForTimeout(2000)

    console.log("  -> Selesai")
    const selesai = await page.waitForXPath(".//*[text()[contains(., 'Selesai')]]")
    await selesai.click()
    await page.waitForTimeout(2000)
}


const accounts = fssync.readdirSync("./accounts").filter(x => x.endsWith(".json"))

if (accounts.length == 0) {
    console.log("Oppss, tidak dapat menemukan cookies di folder ./accounts/")
} else {

    /* jalankan browser secara headless */
    puppeteer.launch({
        headless: false,
        args: [
            "--no-sandbox",
            "--disable-gpu",
        ]
    }).then(async browser => {

        let url = 'https://www.facebook.com/187166408592742/posts/pfbid02wmwNQqX4xKWv8Xeka3B57s51MmC8gQkyeZC8KKbcqcPM7bBFPsQmDwzQr5f3RUitl/?d=n'


        /* ganti subdomain menjadi mbasic */
        url = url.replace(/\/[^f]+/, "//mbasic.")

        const page = await browser.newPage()
        await page.setViewport({ width: 600, height: 800 })

        for (var i = 0; i < accounts.length; i++) {
            const filename = `accounts/${accounts[i]}`
            console.log(`Muat cookies: ${filename}`)

            await loadCookie(page, filename)
            console.log("Membuka link postingan.")
            await page.goto(url)
            await page.waitForTimeout(5000)


            if (await likePost(page)) {
                console.log("Postingan berhasil disukai.")
            } else {
                console.log("Postingan gagal disukai.")
            }

            if (await commentPost(page, "ok")) {
                console.log("Postingan berhasil dikomentari")
            } else {
                console.log("Postingan gagal dikomentari.")
            }


            try {
                await reportPost(page)
            } catch (err) {
                console.error(err)
            }

            await saveCookie(page, filename)
        }
        console.log(`All done. ✨`)
        await browser.close()
    })
}