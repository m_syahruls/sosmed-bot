/* fungsi untuk mengomentari postingan
 * param @page page instance
 *       @msg  pesan text
 *
 * Note: pastikan settingan bahasa adalah
 *       bahasa indonesia
 */

module.exports = async (page, msg) => {
    try {
        /* cari comment textarea */
        const commentArea = await page.$("#composerInput")
        if (commentArea) {

            /* ketik pesan ke textarea lalu jeda 2 detik */
            await commentArea.type(msg)
            await page.waitForTimeout(2000)

            /* cari tombol kirim */
            const submit = await page.$$("xpath/.//input[@value='Komentari']")
            if (submit[0]) {
                /* kirim komentar lalu jeda 2 detik */
                await submit[0].click()
                await page.waitForTimeout(2000)

                /* kembalikan nilai true / berhasil */
                return true
            }
        }
    } catch (err) {
        console.error(err.message)
    }
    /* selain itu false / gagal */
    await page.waitForTimeout(2000)
    return false
}
