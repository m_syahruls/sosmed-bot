/* fungsi untuk Like postingan
 * param @page  page instance
 *
 * Note: pastikan settingan bahasa adalah
 *       bahasa indonesia untuk menghindari error
 *
 *       - fitur `unlike` untuk sementara dihapus
 *
 * Todo: add try catch statement
 */

module.exports = async(page) => {
    try {
        /* cari tombol suka */
        const like = await page.$$("xpath/.//*[text()[contains(., 'Suka')]]")
        if (like[0]) {
            /* click lalu beri jeda 2 detik */
            await like[0].click()
            await page.waitForTimeout(2000)


            /* cari tombol tutup menu
             * tombol ini muncul jika postingan sudah pernah
             * disukai sebelumnnya
             */
            const tutupMenu = await page.$$("xpath/.//*[text()[contains(., 'Tutup')]]")
            if (tutupMenu[0]) {
                /* click untuk menutup menu lalu jeda 2 detik*/
                await tutupMenu[0].click()
                await page.waitForTimeout(2000)
            }

            /* kembalikan nilai true / berhasil */
            return true
        }
    } catch (err) {
        console.error(err.message)
    }
    /* selain itu false / gagal */
    await page.waitForTimeout(2000)
    return false
}
