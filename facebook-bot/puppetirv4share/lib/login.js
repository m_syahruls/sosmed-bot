
/* Function for account login
 * @params page      page instance
 *         username  email/username
 *         password  secret
 */

const EMAIL = "xpath/.//*[@name='email']"
const PASS = "xpath/.//*[@name='pass']"
const SUBMIT = "xpath/.//*[@id='login_form']/ul/li[3]/input"

module.exports = async (page, username, password) => {
    console.log("[+] Trying to log in.")
    try {
        /* clear cookies */
        console.log("    -> Clearing old cookies.")
        const client = await page.target().createCDPSession()
        await client.send('Network.clearBrowserCookies')

        console.log("    -> Opens the login page.")
        await page.goto("https://mbasic.facebook.com/login.php", {waitUntil: "networkidle0"})

        console.log("       -> Fill in the username/email field.")
        await page.type(EMAIL, username)
        console.log("       -> Fill in the password field.")
        await page.type(PASS, password)
        console.log("       -> Submitting login form.")
        await page.click(SUBMIT)

        await page.waitForTimeout(10000)

        let html = await page.content()
        return html.indexOf("login_error") < 0
    } catch (err) {
        console.error(`    -> ${err.message}`)
    }
    await page.waitForTimeout(2000)
    return false
}
