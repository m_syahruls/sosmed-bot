const {default: JSSoup} = require("jssoup")

/* Function for reporting posts
 * @params page  page instance
 *         rKey  keywords
 */
const OPTIONS = "xpath/.//a[@href[contains(., 'direct_actions')]][@class]"
const RESOLVE_PROBLEM = "xpath/.//input[@value='RESOLVE_PROBLEM']"
const SUBMIT = "xpath/.//input[@name='submit' or @name='action']"
const CHECKBOX = "xpath/.//*[@id='m_check_list_aria_label']/input"
const DONE = "xpath/.//*[@href[contains(., '&id=')]]"

module.exports = async (page, rKey) => {
    console.log("    -> Trying to report a post.")
    await page.waitForTimeout(2000)

    try {

        /* Click then pause for 2 seconds */
        console.log("       -> Open more options.")
        await page.click(OPTIONS)
        await page.waitForTimeout(2000)

        /* Click then pause for 2 seconds */
        console.log("       -> Select: report this post.")
        await page.click(RESOLVE_PROBLEM)
        await page.waitForTimeout(2000)

        /* Click then pause for 2 seconds */
        console.log("       -> Submiting.")
        await page.click(SUBMIT)
        await page.waitForTimeout(2000)

        /* Loop to find a value that matches the keyword, if not found select random.
         * Repeat until there are no more options
         */
        let first = true;
        while (true) {
            const soup = new JSSoup(await page.content())
            let action_keys = soup.findAll("input", {name: "tag"})
            let values = []
            let action_index = null

            let i = 0;
            for (action of action_keys) {
                const value = action.attrs.value
                values.push(value)

                if (first && rKey != "" && value.toLowerCase().indexOf(rKey.toLowerCase()) >= 0) {
                    action_index = i + 1
                    first = false
                }
                i++
            }

            if (Object.keys(values).length == 0) break
            if (!action_index) action_index = Math.floor(Math.random() * Object.keys(values).length)

            /* Create xpath based on the selected options */
            console.log(`       -> Select: ${values[action_index - 1]}.`)
            const xpath = "xpath/.//input[@value='" + values[action_index - 1] + "']"

            /* Click then pause for 2 seconds */
            await page.click(xpath)
            await page.waitForTimeout(2000)

            /* Click then pause for 2 seconds */
            console.log("       -> Submiting.")
            await page.click(SUBMIT)
            await page.waitForTimeout(2000)
        }

        /* Click then pause for 2 seconds */
        console.log("       -> Click checkbox.")
        await page.click(CHECKBOX)
        await page.waitForTimeout(2000)

        /* Click then pause for 3 seconds*/
        console.log("       -> Submiting.")
        await page.click(SUBMIT)
        await page.waitForTimeout(3000)

        /* Click then pause for 2 seconds */
        console.log("       -> Click done.")
        await page.click(DONE)
        await page.waitForTimeout(2000)

        console.log("    -> Successfully report a post.")
        return
    } catch (err) {
        console.error(`    -> Error: ${err}`)
    }
    console.log("    -> Failed to report a post.")
}


