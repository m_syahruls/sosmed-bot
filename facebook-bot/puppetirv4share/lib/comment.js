/* Function to comment on posts.
 * @params page page instance
 *         msg  message
 */

const TEXTAREA = "#composerInput"
const SUBMIT_BUTTON = "xpath/.//*[@action[contains(., 'comment')]][1]//input[@type='submit']"

module.exports = async (page, msg) => {
    console.log("    -> Trying to add a comment")
    try {
        /* searching for textarea */
        console.log("       -> Searching for textarea")
        const commentArea = await page.$(TEXTAREA)
        if (commentArea) {

            /* Type a message and pause for 3 seconds */
            console.log(`       -> Message: ${msg}`)
            await commentArea.type(msg)
            await page.waitForTimeout(3000)

            /* Search for the submit button */
            const submit = await page.$$(SUBMIT_BUTTON)
            if (submit[0]) {
                /* Click submit and pause for 3 seconds */
                console.log("       -> Submitting a comment")
                await submit[0].click()
                await page.waitForTimeout(3000)

                /* ok */
                console.log("    -> Successfully added a comment.")
                return
            }
        }
    } catch (err) {
        console.error(`    -> Error: ${err.message}`)
    }

    await page.waitForTimeout(2000)
    console.log("    -> Failed to add a comment.")
}
