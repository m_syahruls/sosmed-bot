const SeleniumStealth = require('selenium-stealth');

const { By, Key, Builder, Capabilities } = require('selenium-webdriver');

var chrome = require('selenium-webdriver/chrome');

require('chromedriver');

var options = new chrome.Options();

// options.addArguments('--headless');
// options.addArguments('--no-gpu');

options.addArguments(
  'user-data-dir=/Users/faisal/Library/Application Support/Google/Chrome'
);
options.addArguments('profile-directory=Default');
options.addArguments('--headless');
options.addArguments('--no-sandbox');
options.addArguments('--disable-dev-shm-usage'); // overcome limited resource problems

const chromeCapabilities = Capabilities.chrome();

async function example() {
  var searchString = 'Automation testing with Selenium';
  const { email, pass } = {
    email: 'miftaahulkhoir@yopmail.com',
    pass: '1Sampe10!',
  };

  let driver = await new Builder()
    .usingServer('http://192.168.64.2:4444')
    .withCapabilities(chromeCapabilities)
    .build();

  //To wait for browser to build and launch properly

  //To fetch http://google.com from the browser with our code.

  try {
    await driver.get('https://www.tiktok.com/login/phone-or-email/email');
    const emailField = await driver.findElement(By.css('[name="username"]'));
    emailField.sendKeys(email);
    const passwordField = await driver.findElement(By.css('[type="password"]'));
    passwordField.sendKeys(pass);
    const buttonSubmit = await driver.findElement(
      By.css('[data-e2e="login-button"]')
    );
    buttonSubmit.click();
  } catch (error) {
    console.log('called', error);
    await driver.quit();
  }

  //To send a search query by passing the value in searchString.
  // await driver.findElement(By.name('q')).sendKeys(searchString, Key.RETURN);

  //Verify the page title and print it
  // var title = await driver.getTitle();
  // console.log('Title is:', title);

  //It is always a safe practice to quit the browser after execution
  // await driver.quit();
}

example();
