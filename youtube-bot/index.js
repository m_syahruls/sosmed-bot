const { By, Key, Builder, WebElement } = require('selenium-webdriver');
const firefox = require('selenium-webdriver/firefox');
const options = new firefox.Options();

async function example() {
    try {
        options.setProfile("./SeleniumWeb")

        const driver = new Builder()
            .forBrowser("firefox")
            .setFirefoxOptions(options)
            .build();

        await driver.get("https://www.youtube.com/watch?v=r5n1GCY-nYQ")

        // const SignIN = await driver.findElement(
        //     By.css('[aria-label="Sign in"]')
        // )

        // const SendEmail = await driver.findElement(
        //     By.name("identifier")).sendKeys("fajarherdian23@gmail.com")

        // const report = await driver.findElement(
        //     By.css('[style-target="host"]')
        // )

        // const Like = await driver.findElement(
        //     By.className("style-scope ytd-segmented-like-dislike-button-renderer")
        // );

        // const Like = await driver.findElement(
        //     By.id('segmented-like-button')
        // );

        // const Dislike = driver.findElement(
        //     By.css('[aria-label="Dislike this video"]')
        // );

        // const moreButton = driver.findElement(
        //     By.css('[style-target="button"]')
        // );

        // const moreButton = driver.findElement(
        //     By.css('menu-active')
        // )

        const comment = driver.findElement(
            By.className("style-scope ytd-comment-simplebox-renderer")
        )
        // const moreButton = await driver.findElement(
        //     By.className('yt-spec-touch-feedback-shape__stroke')
        // );

        const actions = driver.actions();
        // await actions.move(moreButton).click().perform();
        await actions.move(comment).click().perform()
        // await actions.move(report).click().perform()
        // await actions.move(signIN).click().perform();
        // await actions.move(SendEmail).perform();
        // await actions.move(Like).click().perform();
        // await actions.move(Dislike).click().perform();
    } catch (error) {
        console.log('called', error);
    }
}

example();
