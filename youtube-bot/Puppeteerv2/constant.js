const xpathList = {
  dislike:
    "xpath//html/body/ytd-app/div[1]/ytd-page-manager/ytd-watch-flexy/div[5]/div[1]/div/div[2]/ytd-watch-metadata/div/div[2]/div[2]/div/div/ytd-menu-renderer/div[1]/ytd-segmented-like-dislike-button-renderer/div[2]",
  reportWait:
    "xpath//html/body/ytd-app/div[1]/ytd-page-manager/ytd-watch-flexy/div[5]/div[1]/div/div[2]/ytd-watch-metadata/div/div[2]/div[2]/div/div/ytd-menu-renderer/yt-button-shape",
  reportClick:
    "xpath//html/body/ytd-app/ytd-popup-container/tp-yt-iron-dropdown/div/ytd-menu-popup-renderer/tp-yt-paper-listbox/ytd-menu-service-item-renderer[1]",
  reportChoice:
    "xpath//html/body/ytd-app/ytd-popup-container/tp-yt-paper-dialog/yt-report-form-modal-renderer/tp-yt-paper-dialog-scrollable/div/yt-report-form-modal-content/div/yt-options-renderer/div/tp-yt-paper-radio-group/tp-yt-paper-radio-button[6]",
  reportClick2:
    "xpath//html/body/ytd-app/ytd-popup-container/tp-yt-paper-dialog/yt-report-form-modal-renderer/div/yt-button-renderer[2]",
  reportInput:
    "xpath//html/body/ytd-app/ytd-popup-container/tp-yt-paper-dialog[2]/yt-report-details-form-renderer/yt-report-details-form-content/div/div/tp-yt-paper-input-container/div[2]/div/tp-yt-iron-autogrow-textarea",
  reportType:
    "xpath//html/body/ytd-app/ytd-popup-container/tp-yt-paper-dialog[2]/yt-report-details-form-renderer/yt-report-details-form-content/div/div/tp-yt-paper-input-container/div[2]/div/tp-yt-iron-autogrow-textarea/div[2]/textarea",
  reportSubmit:
    "xpath//html/body/ytd-app/ytd-popup-container/tp-yt-paper-dialog[2]/yt-report-details-form-renderer/div[2]/div[2]",
  reportClose:
    "xpath//html/body/ytd-app/ytd-popup-container/tp-yt-paper-dialog[3]/yt-fancy-dismissible-dialog-renderer/div/yt-button-renderer",
  clickComment:
    "xpath//html/body/ytd-app/div[1]/ytd-page-manager/ytd-watch-flexy/div[5]/div[1]/div/div[2]/ytd-comments/ytd-item-section-renderer/div[1]/ytd-comments-header-renderer/div[5]/ytd-comment-simplebox-renderer/div[1]",
  typeComment:
    "xpath//html/body/ytd-app/div[1]/ytd-page-manager/ytd-watch-flexy/div[5]/div[1]/div/div[2]/ytd-comments/ytd-item-section-renderer/div[1]/ytd-comments-header-renderer/div[5]/ytd-comment-simplebox-renderer/div[3]/ytd-comment-dialog-renderer/ytd-commentbox/div[2]/div/div[2]/tp-yt-paper-input-container/div[2]/div/div[1]/ytd-emoji-input/yt-user-mention-autosuggest-input/yt-formatted-string/div",
  submitComment:
    "xpath//html/body/ytd-app/div[1]/ytd-page-manager/ytd-watch-flexy/div[5]/div[1]/div/div[2]/ytd-comments/ytd-item-section-renderer/div[1]/ytd-comments-header-renderer/div[5]/ytd-comment-simplebox-renderer/div[3]/ytd-comment-dialog-renderer/ytd-commentbox/div[2]/div/div[4]/div[5]/ytd-button-renderer[2]",
  like:
    "xpath//html/body/ytd-app/div[1]/ytd-page-manager/ytd-watch-flexy/div[5]/div[1]/div/div[2]/ytd-watch-metadata/div/div[2]/div[2]/div/div/ytd-menu-renderer/div[1]/ytd-segmented-like-dislike-button-renderer/div[1]/ytd-toggle-button-renderer/yt-button-shape/button/yt-touch-feedback-shape/div/div[1]",
  nextEmail: "Enter",
  nextpassword: "Enter"
};

const PositifComment = {
  pos1: "bagus sekali, konten ini sangat menghibur!",
  pos2: "menarik, saya rasa saya akan subscribe akun ini",
  pos3: "Oke ini sangat bermanfaat!",
  pos4: "tes komen"
}

const NegatifComment = {
  neg1: "Jelek, tidak berfaedah!",
  neg2: "Ampas, isinya hoax semua!",
  neg3: "ini tidak dapat dibiarkan, konten ini dapat menganggu banyak orang!"
}

const Report = {
  Rep1: "Konten berbahaya, terdapat isue hoax!",
  Rep2: "Ini ujaran kebencian",
  Rep3: "Sara dan banyak fitnah dibalik konten ini!"
}

exports.xpathList = xpathList;
exports.PositifComment = PositifComment;
exports.NegatifComment = NegatifComment;
exports.Report = Report;

