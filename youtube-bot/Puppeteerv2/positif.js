const { keyboard } = require("puppeteer");
const puppeteer = require("puppeteer-extra");
const StealthPlugin = require("puppeteer-extra-plugin-stealth");
const dotenv = require("dotenv");
// const xpathList = require("./constant");
const { xpathList, PositifComment, NegatifComment } = require("./constant");

puppeteer.use(StealthPlugin());
dotenv.config();
const fs = require("fs");

const COOKIE_PATH = "./helper/cookie.json";
const url =
  "https://www.youtube.com/watch?v=r00ikilDxW4&list=RDIjxEB-oM2WQ&index=2";
// const url = "https://www.youtube.com/watch?v=PIh2xe4jnpk&ab_channel=ournameismagicVEVO";
const LOGIN_GMAIL = "https://accounts.google.com/ServiceLogin";
const EMAIL_SELECTOR = 'input[type="email"]';
const PASSWORD_SELECTOR = 'input[type="password"]';

(async () => {
  const browser = await puppeteer.launch({
    headless: false,
    args: [
      "--no-sandbox",
      "--disable-gpu",
      "--enable-webgl",
      // "--start-maximized",
      "--window-size=1920,1080",
    ],
  });

  const page = await browser.newPage();
  const cookiesFile = "./helper/cookie.json";
  let cookies = [];
  if (fs.existsSync(cookiesFile)) {
    cookies = JSON.parse(fs.readFileSync(cookiesFile));
  }
  if (cookies.length !== 0) {
    // Set cookies from file
    await page.setCookie(...cookies);
    console.log("Memanggil sesion cookies yang tersedia");
  } else {
    // Go to login page and wait for user to login
    await page.goto(LOGIN_GMAIL);

    // Fill login form and submit
    await page.waitForTimeout(3000);
    await page.type(EMAIL_SELECTOR, process.env.EMAIL);
    await page.keyboard.press(xpathList.nextEmail);

    await page.waitForTimeout(2000);
    await page.type(PASSWORD_SELECTOR, process.env.PASSWORD);
    await page.keyboard.press(xpathList.nextpassword);
    await page.waitForTimeout(3000);
    await page.goto(url);

    // Wait for user to login and get the cookies
    await page.waitForNavigation({ waitUntil: "networkidle0" });
    const sessionCookies = await page.cookies();
    console.log("cookie sedang dibuat");

    // Save the cookies to file
    fs.writeFileSync(cookiesFile, JSON.stringify(sessionCookies));
    console.log("cookie sudah dibuat pada", cookiesFile);
  }
  await page.goto(url);
  await page.waitForNavigation({ waitUntil: "networkidle2" });
  await page.waitForTimeout(1000);

  const isLike = async (page) => {
    await page.waitForSelector("#segmented-like-button > ytd-toggle-button-renderer > yt-button-shape > button > div.yt-spec-button-shape-next__icon > yt-icon > yt-animated-icon > ytd-lottie-player > lottie-component > svg > g > g:nth-child(2) > g:nth-child(1)");
    const likeButton = await page.$("#segmented-like-button > ytd-toggle-button-renderer > yt-button-shape > button > div.yt-spec-button-shape-next__icon > yt-icon > yt-animated-icon > ytd-lottie-player > lottie-component > svg > g > g:nth-child(2) > g:nth-child(1)");
    const isDisplayed = await page.evaluate((el) => el.style.display === 'block', likeButton);
    return isDisplayed;

  };
  const isUnLike = async (page) => {
    await page.waitForSelector("#segmented-like-button > ytd-toggle-button-renderer > yt-button-shape > button > div.yt-spec-button-shape-next__icon > yt-icon > yt-animated-icon > ytd-lottie-player > lottie-component > svg > g > g:nth-child(2) > g:nth-child(1)");
    const likeButton = await page.$("#segmented-like-button > ytd-toggle-button-renderer > yt-button-shape > button > div.yt-spec-button-shape-next__icon > yt-icon > yt-animated-icon > ytd-lottie-player > lottie-component > svg > g > g:nth-child(2) > g:nth-child(1)");
    const isDisplayed = await page.evaluate((el) => el.style.display === 'none', likeButton);
    return isDisplayed;
  };
  const isLiked = await isLike(page);
  const isUnLiked = await isUnLike(page);

  if (isLiked) {
    // Comment flow
    await page.waitForTimeout(1000);
    await page.keyboard.press("PageDown");
    await page.waitForSelector(xpathList.clickComment);
    await page.click(xpathList.clickComment);
    await page.waitForSelector(xpathList.typeComment);
    await page.type(xpathList.typeComment, PositifComment.pos4);
    await page.screenshot({ path: `screenshoot/PositifFLow/TypeComment.jpeg` });
    await page.click(xpathList.submitComment);
    await page.waitForTimeout(1500);
    await page.screenshot({ path: `screenshoot/PositifFLow/Comment.jpeg` });
    console.log("comment aja")
  } else if (isUnLiked) {
    // Like flow
    await page.waitForTimeout(1000);
    await page.waitForSelector(xpathList.like);
    await page.click(xpathList.like);
    await page.waitForTimeout(1000);
    await page.screenshot({ path: `screenshoot/PositifFLow/Like.jpeg` });
    await page.waitForTimeout(1000);
    await page.keyboard.press("PageDown");
    await page.waitForSelector(xpathList.clickComment);
    await page.click(xpathList.clickComment);
    await page.waitForSelector(xpathList.typeComment);
    await page.type(xpathList.typeComment, PositifComment.pos4);
    await page.screenshot({ path: `screenshoot/PositifFLow/TypeComment.jpeg` });
    await page.click(xpathList.submitComment);
    await page.waitForTimeout(1500);
    await page.screenshot({ path: `screenshoot/PositifFLow/Comment.jpeg` });
    console.log("Like and comment")
  } else {
    console.log("none")
  }

  await page.waitForTimeout(1500);
  await page.close();
})();