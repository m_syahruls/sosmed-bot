const { keyboard } = require("puppeteer");
const puppeteer = require("puppeteer-extra");
const StealthPlugin = require("puppeteer-extra-plugin-stealth");
const dotenv = require("dotenv");
// const xpathList = require("./constant");
const { xpathList, Report, NegatifComment } = require("./constant");

puppeteer.use(StealthPlugin());
dotenv.config();
const fs = require("fs");

const COOKIE_PATH = "./cookie.json";

const url =
  "https://www.youtube.com/watch?v=PIh2xe4jnpk&ab_channel=ournameismagicVEVO";
const LOGIN_GMAIL = "https://accounts.google.com/ServiceLogin";
const EMAIL_SELECTOR = 'input[type="email"]';
const PASSWORD_SELECTOR = 'input[type="password"]';

(async () => {
  const browser = await puppeteer.launch({
    headless: false,
    args: [
      "--no-sandbox",
      "--disable-gpu",
      "--enable-webgl",
      // "--start-maximized",
      "--window-size=1920,1080",
    ],
  });
  const page = await browser.newPage();
  const cookiesFile = "./helper/cookie.json";
  let cookies = [];
  if (fs.existsSync(cookiesFile)) {
    cookies = JSON.parse(fs.readFileSync(cookiesFile));
  }
  if (cookies.length !== 0) {
    // Set cookies from file
    await page.setCookie(...cookies);
    console.log("Memanggil sesion cookies yang tersedia");
  } else {
    // Go to login page and wait for user to login
    await page.goto(LOGIN_GMAIL);

    // Fill login form and submit
    await page.waitForTimeout(3000);
    await page.type(EMAIL_SELECTOR, process.env.EMAIL);
    await page.keyboard.press(xpathList.nextEmail);

    await page.waitForTimeout(3000);
    await page.type(PASSWORD_SELECTOR, process.env.PASSWORD);
    await page.keyboard.press(xpathList.nextpassword);
    await page.waitForTimeout(4000);
    await page.goto(url);

    // Wait for user to login and get the cookies
    await page.waitForNavigation({ waitUntil: "networkidle0" });
    const sessionCookies = await page.cookies();
    console.log("cookie sedang dibuat");

    // Save the cookies to file
    fs.writeFileSync(cookiesFile, JSON.stringify(sessionCookies));
    console.log("cookie sudah dibuat pada", cookiesFile);
  }

  // Go to URL Yt
  await page.goto(url);
  await page.waitForNavigation({ waitUntil: "networkidle2" });
  await page.waitForTimeout(1000);

  // Dislike YT
  await page.waitForSelector(xpathList.dislike);
  await page.click(xpathList.dislike);
  await page.waitForTimeout(1000);
  await page.screenshot({ path: `screenshoot/NegatifFlow/Dislike.jpeg` });

  // Report
  await page.waitForSelector(xpathList.reportWait);
  await page.click(xpathList.reportWait);
  await page.screenshot({ path: `screenshoot/NegatifFlow/Rep1.jpeg` });
  await page.waitForSelector(xpathList.reportClick);
  await page.click(xpathList.reportClick);
  await page.screenshot({ path: `screenshoot/NegatifFlow/Rep2.jpeg` });
  await page.waitForSelector(xpathList.reportChoice);
  await page.click(xpathList.reportChoice);
  await page.screenshot({ path: `screenshoot/NegatifFlow/Rep3.jpeg` });
  await page.waitForSelector(xpathList.reportClick2);
  await page.click(xpathList.reportClick2);
  await page.screenshot({ path: `screenshoot/NegatifFlow/Rep4.jpeg` });
  await page.waitForSelector(xpathList.reportInput);
  await page.click(xpathList.reportInput);
  await page.screenshot({ path: `screenshoot/NegatifFlow/Rep5.jpeg` });
  await page.waitForSelector(xpathList.reportType);
  await page.type(xpathList.reportType, Report.Rep1);
  await page.screenshot({ path: `screenshoot/NegatifFlow/Rep6.jpeg` });
  await page.waitForSelector(xpathList.reportSubmit);
  await page.click(xpathList.reportSubmit);
  await page.screenshot({ path: `screenshoot/NegatifFlow/Rep7.jpeg` });
  await page.waitForTimeout(1000);
  await page.screenshot({ path: `screenshoot/NegatifFlow/Report.jpeg` });
  await page.waitForSelector(xpathList.reportClose);
  await page.click(xpathList.reportClose);

  //Comment
  await page.waitForTimeout(2000);
  await page.keyboard.press("PageDown");
  await page.screenshot({ path: `screenshoot/NegatifFlow/PgDown.jpeg` });
  await page.waitForSelector(xpathList.clickComment);
  await page.click(xpathList.clickComment);
  await page.waitForSelector(xpathList.typeComment);
  await page.type(xpathList.typeComment, NegatifComment.neg2);
  await page.screenshot({ path: `screenshoot/NegatifFlow/TypeComment.jpeg` });
  await page.click(xpathList.submitComment);
  await page.waitForTimeout(1000);
  await page.screenshot({ path: `screenshoot/NegatifFlow/Comment.jpeg` });
  await page.waitForTimeout(1000);
  await page.close();
})();
