const { By, Key, Builder, until } = require("selenium-webdriver");
const chrome = require("selenium-webdriver/Chrome");
const options = new chrome.Options();

async function example() {
    try {
        const driver = new Builder()
            .forBrowser("chrome")
            .setChromeOptions(options)
            .build();
        await driver.get("https://www.youtube.com/watch?v=r5n1GCY-nYQ");
        await driver.wait(until.elementLocated(By.id("top-level-buttons-computed")), 1000);
        await driver.findElement(
            By.css('[aria-label="Dislike this video"]')
        ).click().perform();
    } catch (error) {
        console.log("called", error);
    }
}

example();
