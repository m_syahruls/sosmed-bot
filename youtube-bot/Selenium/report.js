const { By, Key, Builder } = require("selenium-webdriver");
const chrome = require("selenium-webdriver/Chrome");
const options = new chrome.Options();

const driver = new Builder()
  .forBrowser("chrome")
  .setChromeOptions(options)
  .build();
(async function example() {
  try {
    // Akses URL yang ingin Anda kunjungi dengan Selenium
    await driver.get("https://www.youtube.com/watch?v=r5n1GCY-nYQ");

    await driver.sleep(5000);

    // Lakukan klik pada tombol "More actions"
    await driver.findElement(By.id("button-shape")).click();
    await driver.sleep(3000);

    await driver
      .findElement(By.className("style-scope ytd-menu-popup-renderer"))
      .click();
    await driver.sleep(3000);
  } finally {
    // Tutup browser
    // await driver.quit();
  }
})();
