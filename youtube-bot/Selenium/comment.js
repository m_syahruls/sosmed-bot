const { By, Key, Builder, until } = require("selenium-webdriver");
const chrome = require("selenium-webdriver/Chrome");
const options = new chrome.Options();

async function example() {
  try {
    const driver = new Builder()
      .forBrowser("chrome")
      .setChromeOptions(options)
      .build();

    await driver.get("https://www.youtube.com/watch?v=r5n1GCY-nYQ");
    await driver.sleep(5000);

    // Tunggu hingga video selesai dimuat
    await driver.wait(until.elementLocated(By.css(".html5-main-video")));

    // Scroll ke bawah untuk melihat tombol "Subscribe"
    await driver.executeScript("window.scrollBy(0, 500)");

    // Tunggu hingga tombol "Comment" tersedia. (Click Comment)
    await driver.wait(until.elementLocated(By.css("#placeholder-area")));

    const actions = driver.actions();

    // await driver.wait(until.elementLocated(By.css("#placeholder-area"));
    await driver.findElement(By.css("#placeholder-area")).click().perform();
  } catch (error) {
    console.log("called", error);
  }
}
example();
