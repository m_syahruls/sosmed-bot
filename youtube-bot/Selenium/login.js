const webdriver = require("selenium-webdriver");
require("dotenv").config();

const email = process.env.USER;
const password = process.env.PASSWORD;
console.log(`email: ${email}`);
console.log(`Password: ${password}`);

// console.log("username: ${email}");
// console.log("passoword: ${password}");

// buat instance dari Chrome driver
const driver = new webdriver.Builder().forBrowser("chrome").build();

// buka halaman login Google
driver.get(
  "https://accounts.google.com/v3/signin/identifier?dsh=S2064005352%3A1679627139784242&continue=https%3A%2F%2Faccounts.google.com%2F%3Fhl%3Did&followup=https%3A%2F%2Faccounts.google.com%2F%3Fhl%3Did&hl=id&ifkv=AQMjQ7Q5wDN3eacDaHtDoDNOiSrhBkkcblwPoxZFp0ZHKyMRG7oUrTLcEu9Qnvsxo6q6HMMdpy0VyA&passive=1209600&flowName=GlifWebSignIn&flowEntry=ServiceLogin"
);

// tunggu hingga halaman login muncul
driver.wait(webdriver.until.titleContains("Google"), 5000);

// masukkan email pada input form
driver.findElement(webdriver.By.id("identifierId")).sendKeys(email);
driver.findElement(webdriver.By.id("identifierNext")).click();

// tunggu hingga halaman password muncul
// driver.wait(
//   webdriver.until.elementLocated(webdriver.By.name("password")),
//   5000
// );
// driver.findElement(webdriver.By.name("password")).sendKeys(password);
// driver.findElement(webdriver.By.id("passwordNext")).click();
const iframe = driver.wait(
  webdriver.until.elementLocated(webdriver.By.css("iframe")),
  10000
);
driver.switchTo().frame(iframe);
const passwordInput = driver.wait(
  webdriver.until.elementLocated(webdriver.By.name("password")),
  10000
);

passwordInput.sendKeys(password);
// tunggu hingga halaman utama Google muncul
driver.wait(webdriver.until.titleContains("Google"), 5000);

// tutup browser
// driver.quit();
