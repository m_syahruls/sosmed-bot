const xpathList = {
  dislike:
    "xpath//html/body/ytd-app/div[1]/ytd-page-manager/ytd-watch-flexy/div[5]/div[1]/div/div[2]/ytd-watch-metadata/div/div[2]/div[2]/div/div/ytd-menu-renderer/div[1]/ytd-segmented-like-dislike-button-renderer/div[2]",
  reportWait:
    "xpath//html/body/ytd-app/div[1]/ytd-page-manager/ytd-watch-flexy/div[5]/div[1]/div/div[2]/ytd-watch-metadata/div/div[2]/div[2]/div/div/ytd-menu-renderer/yt-button-shape",
  reportClick:
    "xpath//html/body/ytd-app/ytd-popup-container/tp-yt-iron-dropdown/div/ytd-menu-popup-renderer/tp-yt-paper-listbox/ytd-menu-service-item-renderer[1]",
  reportChoice:
    "xpath//html/body/ytd-app/ytd-popup-container/tp-yt-paper-dialog/yt-report-form-modal-renderer/tp-yt-paper-dialog-scrollable/div/yt-report-form-modal-content/div/yt-options-renderer/div/tp-yt-paper-radio-group/tp-yt-paper-radio-button[6]",
  reportClick2:
    "xpath//html/body/ytd-app/ytd-popup-container/tp-yt-paper-dialog/yt-report-form-modal-renderer/div/yt-button-renderer[2]",
  reportInput:
    "xpath//html/body/ytd-app/ytd-popup-container/tp-yt-paper-dialog[2]/yt-report-details-form-renderer/yt-report-details-form-content/div/div/tp-yt-paper-input-container/div[2]/div/tp-yt-iron-autogrow-textarea",
  reportType:
    "xpath//html/body/ytd-app/ytd-popup-container/tp-yt-paper-dialog[2]/yt-report-details-form-renderer/yt-report-details-form-content/div/div/tp-yt-paper-input-container/div[2]/div/tp-yt-iron-autogrow-textarea/div[2]/textarea",
  reportSubmit:
    "xpath//html/body/ytd-app/ytd-popup-container/tp-yt-paper-dialog[2]/yt-report-details-form-renderer/div[2]/div[2]",
  reportClose:
    "xpath//html/body/ytd-app/ytd-popup-container/tp-yt-paper-dialog[3]/yt-fancy-dismissible-dialog-renderer/div/yt-button-renderer",
  clickComment:
    "xpath//html/body/ytd-app/div[1]/ytd-page-manager/ytd-watch-flexy/div[5]/div[1]/div/div[2]/ytd-comments/ytd-item-section-renderer/div[1]/ytd-comments-header-renderer/div[5]/ytd-comment-simplebox-renderer/div[1]",
  typeComment:
    "xpath//html/body/ytd-app/div[1]/ytd-page-manager/ytd-watch-flexy/div[5]/div[1]/div/div[2]/ytd-comments/ytd-item-section-renderer/div[1]/ytd-comments-header-renderer/div[5]/ytd-comment-simplebox-renderer/div[3]/ytd-comment-dialog-renderer/ytd-commentbox/div[2]/div/div[2]/tp-yt-paper-input-container/div[2]/div/div[1]/ytd-emoji-input/yt-user-mention-autosuggest-input/yt-formatted-string/div",
  submitComment:
    "xpath//html/body/ytd-app/div[1]/ytd-page-manager/ytd-watch-flexy/div[5]/div[1]/div/div[2]/ytd-comments/ytd-item-section-renderer/div[1]/ytd-comments-header-renderer/div[5]/ytd-comment-simplebox-renderer/div[3]/ytd-comment-dialog-renderer/ytd-commentbox/div[2]/div/div[4]/div[5]/ytd-button-renderer[2]",
  like: "xpath//html/body/ytd-app/div[1]/ytd-page-manager/ytd-watch-flexy/div[5]/div[1]/div/div[2]/ytd-watch-metadata/div/div[2]/div[2]/div/div/ytd-menu-renderer/div[1]/ytd-segmented-like-dislike-button-renderer/div[1]/ytd-toggle-button-renderer/yt-button-shape/button/yt-touch-feedback-shape/div/div[1]",
  likeElement:
    "#segmented-like-button > ytd-toggle-button-renderer > yt-button-shape > button > div.yt-spec-button-shape-next__icon > yt-icon > yt-animated-icon > ytd-lottie-player > lottie-component > svg > g > g:nth-child(2) > g:nth-child(1)",
  unlikeElement:
    "#segmented-dislike-button > ytd-toggle-button-renderer > yt-button-shape > button",
  profile:
    "xpath//html/body/ytd-app/div[1]/ytd-page-manager/ytd-watch-flexy/div[5]/div[1]/div/div[2]/ytd-watch-metadata/div/div[2]/div[1]/ytd-video-owner-renderer/div[1]/ytd-channel-name/div/div/yt-formatted-string/a",
  subscribe:
    "xpath//html/body/ytd-app/div[1]/ytd-page-manager/ytd-browse/div[3]/ytd-c4-tabbed-header-renderer/tp-yt-app-header-layout/div/tp-yt-app-header/div[2]/div[2]/div/div[1]/div/div[2]/div[2]/ytd-subscribe-button-renderer/yt-button-shape/button",
  nextEmail: "Enter",
  nextpassword: "Enter",
  clickNext:
    "xpath//html/body/div[1]/div[1]/div[2]/div/c-wiz/div/div[2]/div/div[2]/div/div[1]/div",
  clickShare:
    "xpath//html/body/ytd-app/div[1]/ytd-page-manager/ytd-watch-flexy/div[5]/div[1]/div/div[2]/ytd-watch-metadata/div/div[2]/div[2]/div/div/ytd-menu-renderer/div[1]/ytd-button-renderer/yt-button-shape/button",
  clickFb:
    "xpath//html/body/ytd-app/ytd-popup-container/tp-yt-paper-dialog/ytd-unified-share-panel-renderer/div[2]/yt-third-party-network-section-renderer/div[1]/yt-third-party-share-target-section-renderer/div/div/yt-share-target-renderer[3]/button",
  clickTweet:
    "xpath//html/body/ytd-app/ytd-popup-container/tp-yt-paper-dialog/ytd-unified-share-panel-renderer/div[2]/yt-third-party-network-section-renderer/div[1]/yt-third-party-share-target-section-renderer/div/div/yt-share-target-renderer[4]/button/yt-icon",
  clickSubscribe:
    "xpath//html/body/ytd-app/div[1]/ytd-page-manager/ytd-watch-flexy/div[5]/div[1]/div/div[2]/ytd-watch-metadata/div/div[2]/div[1]/div/ytd-subscribe-button-renderer/yt-button-shape/button",
  subscribeElement:
    "#subscribe-button > ytd-subscribe-button-renderer > yt-button-shape > button > div > span",
  clicknextunsubs:
    "xpath//html/body/ytd-app/ytd-popup-container/tp-yt-iron-dropdown/div/ytd-menu-popup-renderer/tp-yt-paper-listbox/ytd-menu-service-item-renderer[4]/tp-yt-paper-item/yt-formatted-string",
  submitUnsubs:
    "xpath//html/body/ytd-app/ytd-popup-container/tp-yt-paper-dialog/yt-confirm-dialog-renderer/div[2]/div[2]/yt-button-renderer[3]/yt-button-shape/button",
  clickUnsubscribe:
    "xpath//html/body/ytd-app/div[1]/ytd-page-manager/ytd-watch-flexy/div[5]/div[1]/div/div[2]/ytd-watch-metadata/div/div[2]/div[1]/div/ytd-subscribe-button-renderer/div[2]/ytd-subscription-notification-toggle-button-renderer-next/yt-button-shape/button/yt-touch-feedback-shape/div",
};

const PositifComment = {
  pos1: "Konten menarik dan akan sangat bermanfaat!",
  pos2: "Konten yang sangat menghibur ",
  pos3: "Ini bagus sekali!",
};

const NegatifComment = {
  neg1: "konten ini sampah!",
  neg2: "Ampas sekali ini konten",
  neg3: "berbaha dan tidak bagus!",
};

const Report = {
  rep1: "Konten berbahaya, terdapat isue hoax!",
  rep2: "Ini ujaran kebencian",
  rep3: "Sara dan banyak fitnah dibalik konten ini!",
};

module.exports.xpathList = xpathList;
exports.PositifComment = PositifComment;
exports.NegatifComment = NegatifComment;
exports.Report = Report;
