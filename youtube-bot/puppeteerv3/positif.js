const puppeteer = require("puppeteer-extra");
const StealthPlugin = require("puppeteer-extra-plugin-stealth");
const dotenv = require("dotenv");
const { xpathList, PositifComment } = require("./constant");
const { loginServices } = require("./services");
puppeteer.use(StealthPlugin());
dotenv.config();
const cookieHelper = require("../../helper/cookie");

/* Get argument from the command line */
const args = process.argv.slice(2)[0];

const main = async (headless) => {
  const browser = await puppeteer.launch({
    headless: headless,
    executablePath:
      "/Applications/Google Chrome.app/Contents/MacOS/Google Chrome",
    args: [
      "--no-sandbox",
      "--disable-gpu",
      "--enable-webgl",
      // "--single-process",
      "--disable-software-rasterizer",
      // "--start-maximized",
      "--window-size=1920,1080",
    ],
  });

  const page = await browser.newPage();
  const ua =
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36";
  await page.setUserAgent(ua);

  const url = "https://www.youtube.com/watch?v=zrGiijJ7oMk";

  //Checking if there a cookie
  const isAuth = await cookieHelper.checkCookie(page);
  if (!isAuth) {
    await loginServices.login(page);
    await cookieHelper.saveCookie(page);
  }
  await cookieHelper.storeCookie(page);

  await page.goto(url);
  await page.waitForNavigation({ waitUntil: "networkidle2" });
  await page.waitForTimeout(1000);

  const isLike = async (page) => {
    await page.waitForSelector(xpathList.likeElement);
    const likeButton = await page.$(xpathList.likeElement);
    const isDisplayed = await page.evaluate(
      (el) => el.style.display === "block",
      likeButton
    );
    return isDisplayed;
  };
  const isNotLiked = async (page) => {
    await page.waitForSelector(xpathList.likeElement);
    const likeButton = await page.$(xpathList.likeElement);
    const isDisplayed = await page.evaluate(
      (el) => el.style.display === "none",
      likeButton
    );
    return isDisplayed;
  };
  const isLiked = await isLike(page);
  const isNotLikedd = await isNotLiked(page);
  const isSubscribe = await page.$(xpathList.subscribeElement);

  if (
    isSubscribe &&
    (await isSubscribe.evaluate((node) => node.textContent === "Subscribe"))
  ) {
    console.log("Flow if not subscribed");
    try {
      await page.waitForSelector(xpathList.clickSubscribe);
      await page.click(xpathList.clickSubscribe);
      console.log("succes subscribed");
    } catch (error) {
      console.log("can't subscribe because :" + error);
    }
  } else if (
    isSubscribe &&
    (await isSubscribe.evaluate(
      (node) =>
        node.textContent === "Subscribed" || node.textContent === "Disubscribe"
    ))
  ) {
    console.log("Flow if subscribed, not click subscribe again!");
  } else {
    console.log("nothing can't do");
  }

  if (isLiked) {
    console.log("Flow if liked");
    try {
      await page.keyboard.press("PageDown");
      await page.waitForTimeout(1000);
      await page.waitForSelector(xpathList.clickComment);
      await page.click(xpathList.clickComment);
      console.log("succes click comment");
    } catch (error) {
      console.log("cant click comment" + error);
    }
    try {
      await page.waitForSelector(xpathList.typeComment);
      await page.type(xpathList.typeComment, PositifComment.pos3);
      await page.click(xpathList.submitComment);
      console.log(`succes comment with : ${PositifComment.pos3}`);
      await page.waitForTimeout(2000);
    } catch (error) {
      console.log("cant type comment with error :" + error);
    }
  } else if (isNotLikedd) {
    console.log("Flow if not liked");
    try {
      await page.waitForSelector(xpathList.like);
      await page.click(xpathList.like);
      await page.waitForTimeout(1000);
      console.log("succes like");
    } catch (error) {
      console.log(error);
    }
    try {
      await page.keyboard.press("PageDown");
      await page.waitForTimeout(1000);
      await page.waitForSelector(xpathList.clickComment);
      await page.click(xpathList.clickComment);
      console.log("succes click comment");
    } catch (error) {
      console.log("cant click comment" + error);
    }
    try {
      await page.waitForSelector(xpathList.typeComment);
      await page.type(xpathList.typeComment, PositifComment.pos3);
      await page.click(xpathList.submitComment);
      console.log(`succes comment with : ${PositifComment.pos3}`);
      await page.waitForTimeout(1000);
    } catch (error) {
      console.log("cant type comment with error :" + error);
    }
  } else {
    console.log("Nothing can do");
  }
  await page.waitForTimeout(2000);
  await browser.close();
}
main(false);
