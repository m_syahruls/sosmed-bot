const fs = require("fs/promises");
const path = "./storage/cookies.json";

const loadCookie = async (page) => {
  return new Promise(async (resolve, reject) => {
    try {
      const cookie = await fs.readFile(path, {
        encoding: "utf-8",
      });
      const cookieParse = JSON.parse(cookie);
      if (cookieParse.length != 0) {
        await page.setCookie(...cookieParse);
        resolve("loadCookie: successfully");
      }
      throw "Invalid cookie";
    } catch (error) {
      reject("loadCookie error: " + error);
    }
  });
};

const saveCookie = async (page) => {
  return new Promise(async (resolve, reject) => {
    try {
      const cookie = await page.cookies();
      await fs.writeFile(path, JSON.stringify(cookie, null, 2));
      resolve("saveCookie: successfully");
    } catch (error) {
      reject("saveCookie error: " + error);
    }
  });
};

module.exports = {
  loadCookie,
  saveCookie,
};
