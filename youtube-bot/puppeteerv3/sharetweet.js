const puppeteer = require("puppeteer-extra");
const StealthPlugin = require("puppeteer-extra-plugin-stealth");

const { xpathList } = require("./constant");

puppeteer.use(StealthPlugin());

(async () => {
  const browser = await puppeteer.launch({
    headless: true,
    args: [
      "--no-sandbox",
      "--disable-gpu",
      "--disable-dev-shm-usage",
      "--disable-setuid-sandbox",
      // "--disable-accelerated-2d-canvas",
      "--disable-gpu-sandbox",
      // "--disable-accelerated-video-decode",
      "--window-size=1920,1080",
    ],
  });

  const page = await browser.newPage();

  // await page.setViewport({ width: 1920, height: 1080 });

  await page.goto(
    "https://www.youtube.com/watch?v=QgnQCIU94oQ&ab_channel=TeguhSuwandi"
  );

  try {
    // Wait for the "Share" button to be visible and click it
    await page.waitForSelector(xpathList.clickShare, { visible: true });
    await page.click(xpathList.clickShare);

    // Wait for the Facebook icon to be visible and click it
    await page.waitForSelector(xpathList.clickTweet, { visible: true });
    await page.click(xpathList.clickTweet);
    console.log("Done click Button Twitter");
  } catch (error) {
    console.log(error);
  } finally {
    // await browser.close();
  }
})();
