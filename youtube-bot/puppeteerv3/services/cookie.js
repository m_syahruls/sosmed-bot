const fs = require("fs/promises");
const path = "./storage/cookies.json";

const loadCookie = async (page) => {
  try {
    const cookie = await fs.readFile(path, {
      encoding: "utf-8",
    });
    await page.setCookie(...JSON.parse(cookie));
    console.log("loadCookie: successfully");
  } catch (error) {
    console.log("loadCookie error: " + error);
  }
};

const saveCookie = async (page) => {
  try {
    const sessionData = await page.cookies();
    await fs.writeFile(path, JSON.stringify(sessionData, null, 2));
    console.log("saveCookie: successfully");
  } catch (error) {
    console.log("saveCookie error: " + error);
  }
};

module.exports = {
  loadCookie,
  saveCookie,
};
