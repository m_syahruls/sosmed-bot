const { xpathList } = require("../constant");
const puppeteer = require("puppeteer-extra");
const LOGIN_GMAIL = "https://accounts.google.com/ServiceLogin";
const url =
  "https://www.youtube.com/watch?v=EXJiVW-GdK4&ab_channel=KOMPASTVJAWATIMUR";
const EMAIL_SELECTOR = 'input[type="email"]';
const PASSWORD_SELECTOR = 'input[type="password"]';

// Fill login form and submit
const login = async (page) => {
  return new Promise(async (resolve, reject) => {
    try {
      await page.goto(LOGIN_GMAIL, { waitUntil: "networkidle2" });
      await page.waitForTimeout(3000);
      await page.type(EMAIL_SELECTOR, process.env.EMAIL);
      // await page.screenshot({ path: `screenshoot/login/email.jpeg` });
      await page.click(xpathList.clickNext);
      // await page.screenshot({ path: `screenshoot/login/clicknextemail.jpeg` });
      await page.waitForTimeout(3000);

      await page.type(PASSWORD_SELECTOR, process.env.PASSWORD);
      // await page.screenshot({ path: `screenshoot/login/isipassword.jpeg` });
      await page.click(xpathList.clickNext);
      await page.screenshot({
        // path: `screenshoot/login/cliknextpassword.jpeg`,
      });
      await page.waitForTimeout(3000);
      await page.goto(url);
      resolve("login: successfully");
    } catch (error) {
      reject("login error: " + error);
    }
    await page.waitForTimeout(1000);
  });
};

module.exports = {
  login,
};
