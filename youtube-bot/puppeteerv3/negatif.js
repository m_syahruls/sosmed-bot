// const { keyboard } = require("puppeteer");
const puppeteer = require("puppeteer-extra");
const StealthPlugin = require("puppeteer-extra-plugin-stealth");
const dotenv = require("dotenv");
// const xpathList = require("./constant");
const { xpathList, NegatifComment, Report } = require("./constant");
const { loginServices } = require("./services");
puppeteer.use(StealthPlugin());
dotenv.config();
const cookieHelper = require("../../helper/cookie");

/* Get argument from the command line */
const args = process.argv.slice(2)[0];

const main = async (headless) => {
  const browser = await puppeteer.launch({
    headless: headless,
    executablePath:
      "/Applications/Google Chrome.app/Contents/MacOS/Google Chrome",
    args: [
      "--no-sandbox",
      "--disable-gpu",
      "--enable-webgl",
      // "--start-maximized",
      "--window-size=1920,1080",
    ],
  });
  const page = await browser.newPage();
  const ua =
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36";
  await page.setUserAgent(ua);

  const url = "https://www.youtube.com/watch?v=-xnqUQQarr0";

  //Checking if there a cookie
  const isAuth = await cookieHelper.checkCookie(page);
  if (!isAuth) {
    await loginServices.login(page);
    await cookieHelper.saveCookie(page);
  }
  await cookieHelper.storeCookie(page);

  await page.goto(url);
  await page.waitForNavigation({ waitUntil: "networkidle2" });
  await page.waitForTimeout(1000);

  const isDislike = async () => {
    const dislikeButton = await page.$(xpathList.unlikeElement);
    return (
      (await dislikeButton.evaluate((el) =>
        el.getAttribute("aria-pressed")
      )) === "true"
    );
  };
  const isntDislike = async () => {
    const dislikeButton = await page.$(xpathList.unlikeElement);
    return (
      (await dislikeButton.evaluate((el) =>
        el.getAttribute("aria-pressed")
      )) === "false"
    );
  };

  const isSubscribe = await page.$(xpathList.subscribeElement);

  if (
    isSubscribe &&
    (await isSubscribe.evaluate(
      (node) =>
        node.textContent === "Subscribed" || node.textContent === "Disubscribe"
    ))
  ) {
    console.log("Flow if subscribed");
    try {
      await page.waitForSelector(xpathList.clickUnsubscribe);
      await page.click(xpathList.clickUnsubscribe);
      await page.waitForSelector(xpathList.clicknextunsubs);
      await page.click(xpathList.clicknextunsubs);
      await page.waitForSelector(xpathList.submitUnsubs);
      await page.click(xpathList.submitUnsubs);
      console.log("succes unsubscribed");
      await page.waitForTimeout(3000);
    } catch (error) {
      console.log("can't unsubscribe because :" + error);
    }
  } else if (
    isSubscribe &&
    (await isSubscribe.evaluate((node) => node.textContent === "Subscribe"))
  ) {
    console.log("Flow if not subsribed");
  } else {
    console.log("nothing can't do");
  }

  if (await isDislike()) {
    console.log("Flow if disliked");
    try {
      console.log("Tryin Report");
      await page.waitForTimeout(1500);
      await page.waitForSelector(xpathList.reportWait);
      await page.click(xpathList.reportWait);
      await page.waitForSelector(xpathList.reportClick);
      await page.click(xpathList.reportClick);
      await page.waitForSelector(xpathList.reportChoice);
      await page.click(xpathList.reportChoice);
      await page.waitForSelector(xpathList.reportClick2);
      await page.click(xpathList.reportClick2);
      await page.waitForSelector(xpathList.reportInput);
      await page.click(xpathList.reportInput);
      await page.waitForSelector(xpathList.reportType);
      await page.waitForTimeout(1500)
      await page.type(xpathList.reportType, Report.rep1);
      await page.waitForSelector(xpathList.reportSubmit);
      await page.click(xpathList.reportSubmit);
      await page.waitForSelector(xpathList.reportClose);
      await page.click(xpathList.reportClose);
      console.log(`succes report with reason : ${Report.rep1}`);
    } catch (error) {
      console.log("cant report because :" + error);
    }
    try {
      await page.waitForTimeout(1500);
      await page.keyboard.press("PageDown");
      await page.waitForTimeout(1000);
      await page.waitForSelector(xpathList.clickComment);
      await page.click(xpathList.clickComment);
      console.log("succes click comment");
    } catch (error) {
      console.log("cant click comment :" + error);
    }
    try {
      await page.waitForTimeout(1500)
      await page.waitForSelector(xpathList.typeComment);
      await page.type(xpathList.typeComment, NegatifComment.neg3);
      await page.click(xpathList.submitComment);
      console.log(`succes comment with : ${NegatifComment.neg3}`);
      await page.waitForTimeout(2000);
    } catch (error) {
      console.log("cant type comment with error :" + error);
    }
  } else if (await isntDislike()) {
    console.log("Flow if not disliked");
    try {
      await page.waitForTimeout(1500)
      await page.waitForSelector(xpathList.dislike);
      await page.click(xpathList.dislike);
      await page.waitForTimeout(1000);
      console.log("succes dislike");
    } catch (error) {
      console.log("cant dislike :" + error);
    }

    try {
      console.log("Tryin Report");
      await page.waitForTimeout(1500);
      await page.waitForSelector(xpathList.reportWait);
      await page.click(xpathList.reportWait);
      await page.waitForSelector(xpathList.reportClick);
      await page.click(xpathList.reportClick);
      await page.waitForSelector(xpathList.reportChoice);
      await page.click(xpathList.reportChoice);
      await page.waitForSelector(xpathList.reportClick2);
      await page.click(xpathList.reportClick2);
      await page.waitForSelector(xpathList.reportInput);
      await page.click(xpathList.reportInput);
      await page.waitForSelector(xpathList.reportType);
      await page.waitForTimeout(1500)
      await page.type(xpathList.reportType, Report.rep1);
      await page.waitForSelector(xpathList.reportSubmit);
      await page.click(xpathList.reportSubmit);
      await page.waitForSelector(xpathList.reportClose);
      await page.click(xpathList.reportClose);
      console.log(`succes report with reason : ${Report.rep1}`);
    } catch (error) {
      console.log("cant report because :" + error);
    }
    try {
      await page.waitForTimeout(1500);
      await page.keyboard.press("PageDown");
      await page.waitForSelector(xpathList.clickComment);
      await page.click(xpathList.clickComment);
      console.log("succes click comment");
    } catch (error) {
      console.log("cant click comment :" + error);
    }
    try {
      await page.waitForTimeout(1500)
      await page.waitForSelector(xpathList.typeComment);
      await page.type(xpathList.typeComment, NegatifComment.neg3);
      await page.click(xpathList.submitComment);
      console.log(`succes comment with : ${NegatifComment.neg3}`);
      await page.waitForTimeout(1000);
    } catch (error) {
      console.log("cant type comment with error :" + error);
    }
  } else {
    console.log("Nothing can do");
  }
  await page.waitForTimeout(2000);
  await browser.close();
}
main(false)
