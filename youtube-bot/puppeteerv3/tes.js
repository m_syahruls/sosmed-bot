const { keyboard } = require("puppeteer");
const puppeteer = require("puppeteer-extra");
const StealthPlugin = require("puppeteer-extra-plugin-stealth");
const dotenv = require("dotenv");
const xpathList = require("./constant");

puppeteer.use(StealthPlugin());
dotenv.config();
const fs = require("fs");

const COOKIE_PATH = "./helper/cookie.json";
const LOGIN_YT =
  "https://www.youtube.com/watch?v=EXJiVW-GdK4&ab_channel=KOMPASTVJAWATIMUR";

(async () => {
  const browser = await puppeteer.launch({
    headless: false,
    args: [
      "--no-sandbox",
      "--disable-gpu",
      "--enable-webgl",
      "--start-maximized",
    ],
  });

  const page = await browser.newPage();
  const cookiesFile = "./helper/cookie.json";
  let cookies = [];
  if (fs.existsSync(cookiesFile)) {
    cookies = JSON.parse(fs.readFileSync(cookiesFile));
  }
  if (cookies.length !== 0) {
    // Set cookies from file
    await page.setCookie(...cookies);
    console.log("Memanggil sesion cookies yang tersedia");
  } else {
    // Go to login page and wait for user to login
    await page.goto(LOGIN_GMAIL);

    // Fill login form and submit
    await page.waitForResponse(3000);
    await page.type(EMAIL_SELECTOR, process.env.EMAIL);
    await page.keyboard.press("Enter");

    await page.waitForResponse(3000);
    await page.type(PASSWORD_SELECTOR, process.env.PASSWORD);
    await page.keyboard.press("Enter");
    await page.waitForResponse(4000);
    await page.goto(LOGIN_YT);

    // Wait for user to login and get the cookies
    await page.waitForNavigation({ waitUntil: "networkidle0" });
    const sessionCookies = await page.cookies();
    console.log("cookie sedang dibuat");

    // Save the cookies to file
    fs.writeFileSync(cookiesFile, JSON.stringify(sessionCookies));
    console.log("cookie sudah dibuat pada", cookiesFile);
  }
  await page.goto(LOGIN_YT);
  // Tunggu hingga semua komentar dimuat
  await page.waitForSelector("#sections #contents", { timeout: 5000 });

  // Ambil teks dari semua komentar
  const comments = await page.$$eval("#content-text", (elements) =>
    elements.map((el) => el.innerText)
  );
  console.log(`Komentar: ${comments.join("\n")}`);

  // Tutup browser
  await browser.close();
})();
