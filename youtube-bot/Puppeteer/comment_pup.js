const { Keyboard } = require('puppeteer');
const puppeteer = require('puppeteer-extra');
const StealthPlugin = require('puppeteer-extra-plugin-stealth');
const dotenv = require('dotenv');

puppeteer.use(StealthPlugin());

dotenv.config();
const EMAIL_SELECTOR = 'input[type="email"]';
const PASSWORD_SELECTOR = 'input[type="password"]';
(async () => {
  const browser = await puppeteer.launch({
    headless: false,
    args: [
      "--no-sandbox",
      "--disable-gpu",
      "--enable-webgl",
      "--start-maximized",
    ],
  });

  const loginUrl =
    "https://accounts.google.com/AccountChooser?service=mail&continue=https://google.com&hl=en";
  // const yt = "https://www.youtube.com"
  // const yt = "https://www.youtube.com/watch?v=bQB3YJVNgsY&t=9s";
  const yt = "https://www.youtube.com/watch?v=PIh2xe4jnpk";
  const page = await browser.newPage();

  // Sign Up Email
  await page.goto(loginUrl, { waitUntil: "networkidle2" });
  await page.type(EMAIL_SELECTOR, process.env.EMAIL);
  await page.waitForSelector(EMAIL_SELECTOR);
  await page.waitForTimeout(2000);
  await page.keyboard.press("Enter");

  // Sign Password
  await page.waitForTimeout(2000);
  // await page.waitForSelector(PASSWORD_SELECTOR)
  await page.type(PASSWORD_SELECTOR, process.env.PASSWORD);
  await page.keyboard.press("Enter");
  // await page.waitForSelector(PASSWORD_SELECTOR)
  await page.waitForTimeout(2000);

  // Sending Comment
  await page.goto(yt);
  await page.waitForSelector("div#segmented-dislike-button");
  await page.waitForTimeout(2500);
  await page.keyboard.press("PageDown");
  await page.waitForTimeout(2500);
  await page.click("div#placeholder-area");
  await page.waitForTimeout(3000);
  await page.type("div#contenteditable-root", "test komentar 123");
  await page.click("ytd-button-renderer#submit-button");
})();
