const { keyboard } = require("puppeteer");
const puppeteer = require("puppeteer-extra");
const StealthPlugin = require("puppeteer-extra-plugin-stealth");
const dotenv = require("dotenv");

puppeteer.use(StealthPlugin());
dotenv.config();
const EMAIL_SELECTOR = 'input[type="email"]';
const PASSWORD_SELECTOR = 'input[type="password"]';

(async () => {
  const browser = await puppeteer.launch({
    headless: false,
    args: [
      "--no-sandbox",
      "--disable-gpu",
      "--enable-webgl",
      "--start-maximized",
    ],
  });
  //membuka link google dan youtube
  const loginUrl =
    "https://accounts.google.com/AccountChooser?service=mail&continue=https://google.com&hl=en";
  const yt = "https://www.youtube.com/watch?v=PIh2xe4jnpk";
  const page = await browser.newPage();

  //signup Email
  await page.goto(loginUrl, { waitUntil: "networkidle2" });
  //   await page.waitForNavigation({ waitUntil: "networkidle2" });

  await page.type(EMAIL_SELECTOR, process.env.EMAIL);
  await page.waitForSelector(EMAIL_SELECTOR);
  await page.waitForTimeout(3000);
  await page.keyboard.press("Enter");

  //sign password
  await page.waitForTimeout(3000);
  //menunggu prosess input password
  await page.type(PASSWORD_SELECTOR, process.env.PASSWORD);
  await page.keyboard.press("Enter");
  //menunggu prosses untuk login
  await page.waitForTimeout(5000);

  // Like YT
  await page.goto(yt);
  await page.waitForSelector("div#segmented-like-button");
  await page.waitForTimeout(3000);
  await page.click("div#segmented-like-button");

  //Comment
  await page.waitForTimeout(3000);
  await page.keyboard.press("PageDown");
  await page.waitForTimeout(2000);
  await page.click("div#placeholder-area");
  await page.waitForTimeout(3000);
  await page.type("div#contenteditable-root", "wah bagus sekali");
  await page.click("ytd-button-renderer#submit-button");
})();
