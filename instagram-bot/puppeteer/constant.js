const reportByHarrashmentPageXpathList = {
  reportRadio1:
    'xpath//html/body/div/div/div[3]/div/div[2]/div/form/div[1]/div[3]/label[2]',
  reportRadio2:
    'xpath//html/body/div/div/div[3]/div/div[2]/div/form/div[4]/div[2]/label[2]',
  reportRadio3:
    'xpath//html/body/div/div/div[3]/div/div[2]/div/form/div[9]/div[2]/label[2]',
  reportInput1:
    'xpath//html/body/div/div/div[3]/div/div[2]/div/form/div[2]/input',
  reportInput2:
    'xpath//html/body/div/div/div[3]/div/div[2]/div/form/div[13]/input',
  reportInput3:
    'xpath//html/body/div/div/div[3]/div/div[2]/div/form/div[14]/input',
  reportInput4:
    'xpath//html/body/div/div/div[3]/div/div[2]/div/form/div[15]/input',
  reportInput5:
    'xpath//html/body/div/div/div[3]/div/div[2]/div/form/div[17]/div[2]/div/div/input',
  reportTextarea1:
    'xpath//html/body/div/div/div[3]/div/div[2]/div/form/div[16]/textarea',
  submitButton: 'xpath//html/body/div/div/div[3]/div/div[2]/div/form/button',
};

const accountXpathList = {
  moreButton:
    'xpath//html/body/div[2]/div/div/div[2]/div/div/div/div[1]/div[1]/div[2]/div[2]/section/main/div/header/section/div[1]/div[3]/button',
  reportButton:
    'xpath//html/body/div[2]/div/div/div[3]/div/div/div[1]/div/div[2]/div/div/div/div/div[2]/div/div/div/button[3]',
  reportItem1:
    'xpath//html/body/div[2]/div/div/div[3]/div/div/div[1]/div/div[2]/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div[3]/button[2]/div',
  reportItem2:
    'xpath//html/body/div[2]/div/div/div[3]/div/div/div[1]/div/div[2]/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div[1]/button[1]/div',
  reportItem3:
    'xpath//html/body/div[2]/div/div/div[3]/div/div/div[1]/div/div[2]/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div[1]/button[6]/div',
  submitButton:
    'xpath//html/body/div[2]/div/div/div[3]/div/div/div[1]/div/div[2]/div/div/div/div/div[2]/div/div/div[2]/div/div/button',
  closeButton:
    'xpath//html/body/div[2]/div/div/div[2]/div/div/div/div[1]/div[1]/section/div[1]/div/section/div/div[1]/div/div/div/div[3]/div',

  followButton:
    'xpath//html/body/div[2]/div/div/div[2]/div/div/div/div[1]/div[1]/div[2]/div[2]/section/main/div/header/section/div[1]/div[2]/div/div[1]/button/div/div',
  followingButton:
    'xpath//html/body/div[2]/div/div/div[2]/div/div/div/div[1]/div[1]/div[2]/div[2]/section/main/div/header/section/div[1]/div[2]/div/div[1]/button/div/div[2]',
  unfollowItem:
    'xpath//html/body/div[2]/div/div/div[3]/div/div/div[1]/div/div[2]/div/div/div/div/div[2]/div/div/div/div[8]/div[1]',
};

const postXpathList = {
  usernameLink:
    'xpath//html/body/div[2]/div/div/div[2]/div/div/div/div[1]/div[1]/div[2]/section/main/div[1]/div[1]/article/div/div[2]/div/div[1]/div/header/div[2]/div[1]/div[1]/div/div/span/div/div/a',
  likeButton:
    'xpath//html/body/div[2]/div/div/div[2]/div/div/div/div[1]/div[1]/div[2]/section/main/div[1]/div[1]/article/div/div[2]/div/div[2]/section[1]/span[1]',
  likeButtonDeactivate:
    'xpath//html/body/div[2]/div/div/div[2]/div/div/div/div[1]/div[1]/div[2]/section/main/div[1]/div[1]/article/div/div[2]/div/div[2]/section[1]/span[1]/button/div[2]',
  commentButton:
    'xpath//html/body/div[2]/div/div/div[2]/div/div/div/div[1]/div[1]/div[2]/section/main/div[1]/div[1]/article/div/div[2]/div/div[2]/section[1]/span[2]',
  followButton:
    'xpath//html/body/div[2]/div/div/div[2]/div/div/div/div[1]/div[1]/div[2]/section/main/div[1]/div[1]/article/div/div[1]/div/header/div[2]/div[1]/div[2]/button/div/div',
  followingButton:
    'xpath//html/body/div[2]/div/div/div[2]/div/div/div/div[1]/div[1]/div[2]/section/main/div[1]/div[1]/article/div/div[1]/div/header/div[2]/div[1]/div[2]/button/div/div',

  moreButton:
    'xpath//html/body/div[2]/div/div/div[2]/div/div/div/div[1]/div[1]/div[2]/section/main/div[1]/div[1]/article/div/div[2]/div/div[1]/div/div',
  reportButton:
    'xpath//html/body/div[2]/div/div/div[3]/div/div/div[1]/div/div[2]/div/div/div/div/div[2]/div/div/div/button[1]',
  reportItem:
    'xpath//html/body/div[2]/div/div/div[3]/div/div/div[1]/div/div[2]/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div[3]/button[3]/div',
  submitButton:
    'xpath//html/body/div[2]/div/div/div[3]/div/div/div[1]/div/div[2]/div/div/div/div/div[2]/div/div/div[2]/div/div/button',
  closeButton:
    'xpath//html/body/div[2]/div/div/div[3]/div/div/div[1]/div/div[2]/div/div/div/div/div[2]/div/div/div/div/div/div[4]/button',

  shareButton:
    'xpath//html/body/div[2]/div/div/div[3]/div/div/div[1]/div/div[2]/div/div/div/div/div[2]/div/div/div/button[4]',
  shareFacebook:
    'xpath//html/body/div[2]/div/div/div[3]/div/div/div[1]/div/div[2]/div/div/div/div/div[2]/div/div/div[2]/div/a[2]/div[1]',
  shareTwitter:
    'xpath//html/body/div[2]/div/div/div[3]/div/div/div[1]/div/div[2]/div/div/div/div/div[2]/div/div/div[2]/div/a[4]/div[1]',
  shareLink:
    'xpath//html/body/div[2]/div/div/div[3]/div/div/div[1]/div/div[2]/div/div/div/div/div[2]/div/div/div[2]/div/a[7]/div[1]',

  createButton:
    'xpath//html/body/div[2]/div/div/div[2]/div/div/div/div[1]/div[1]/div[1]/div/div/div/div/div[2]/div[7]/div/div/a/div/div[1]/div/div',
  nextCreateButton:
    'xpath//html/body/div[2]/div/div/div[3]/div/div/div[1]/div/div[3]/div/div/div/div/div[2]/div/div/div/div[1]/div/div/div[3]/div',
  captionInput:
    'xpath//html/body/div[2]/div/div/div[3]/div/div/div[1]/div/div[3]/div/div/div/div/div[2]/div/div/div/div[2]/div[2]/div/div/div/div[2]/div[1]/div[1]/p',
  sharedAlert:
    'xpath//html/body/div[2]/div/div/div[3]/div/div/div[1]/div/div[3]/div/div/div/div/div[2]/div/div/div/div[2]/div[1]/div/div[2]/div/span',
};

const loginXpathList = {
  usernameInput:
    'xpath//html/body/div[2]/div/div/div[1]/div/div/div/div[1]/section/main/article/div[2]/div[1]/div[2]/form/div/div[1]/div/label/input',
  passwordInput:
    'xpath//html/body/div[2]/div/div/div[1]/div/div/div/div[1]/section/main/article/div[2]/div[1]/div[2]/form/div/div[2]/div/label/input',
  submitButton:
    'xpath//html/body/div[2]/div/div/div[1]/div/div/div/div[1]/section/main/article/div[2]/div[1]/div[2]/form/div/div[3]/button',
  errorMessage:
    'xpath//html/body/div[2]/div/div/div[1]/div/div/div/div[1]/section/main/article/div[2]/div[1]/div[2]/form/div[2]',
  homeButton:
    'xpath//html/body/div[2]/div/div/div[2]/div/div/div/div[1]/div[1]/div[1]/div/div/div/div/div[2]/div[1]/div/div/a/div',
};

const storiesXpathList = {
  viewButton:
    'xpath//html/body/div[2]/div/div/div[2]/div/div/div/div[1]/div[1]/section/div[1]/div/section/div/div[1]/div/div/div/div[3]/div',
  moreButton:
    'xpath//html/body/div[2]/div/div/div[2]/div/div/div/div[1]/div[1]/section/div[1]/div/section/div/header/div[2]/div[2]/div/button/div',
  reportButton:
    'xpath//html/body/div[2]/div/div/div[3]/div/div/div[1]/div/div[2]/div/div/div/div/div[2]/div/div/div/button[1]',
  reportItem:
    'xpath//html/body/div[2]/div/div/div[3]/div/div/div[1]/div/div[2]/div/div/div/div/div[2]/div/div/div[2]/div/div/div/div[3]/button[3]/div',
  submitButton:
    'xpath//html/body/div[2]/div/div/div[3]/div/div/div[1]/div/div[2]/div/div/div/div/div[2]/div/div/div[2]/div/div/button',
  closeButton:
    'xpath//html/body/div[2]/div/div/div[3]/div/div/div[1]/div/div[2]/div/div/div/div/div[2]/div/div/div/div/div/div[4]/button',
};

module.exports = {
  reportByHarrashmentPageXpathList,
  accountXpathList,
  postXpathList,
  loginXpathList,
  storiesXpathList,
};
