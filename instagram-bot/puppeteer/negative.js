require('dotenv').config();
const puppeteer = require('puppeteer-extra');
const stealthPlugin = require('puppeteer-extra-plugin-stealth');
const {
  loginServices,
  postServices,
  commentServices,
  accountServices,
  // internetThrottleServices,
} = require('./services');
puppeteer.use(stealthPlugin());
const cookieHelper = require('../../helper/cookie.js');

async function negative(paramHeadless) {
  const browser = await puppeteer.launch({
    headless: paramHeadless,
    args: [
      '--no-sandbox',
      '--disable-gpu',
      '--enable-webgl',
      '--window-size=1920,1080',
    ],
  });
  const page = await browser.newPage();

  /* Throttling the internet connection, 
  change the speed manually inside internetThrottle function. */
  // await internetThrottleServices.internetThrottle(page);

  let comments;
  const email = 'minztergundul@gmail.com';
  const password = '7DosaBesar';
  const postUrl = 'https://www.instagram.com/p/Cqafk-jj5HV/';

  /* It's checking if there is a cookie. */
  const isAuth = await cookieHelper.checkCookie(page);
  if (!isAuth) {
    const loginFunc = await loginServices.login(page, email, password);
    console.log(loginFunc);
  }
  const storeCookieFunc = await cookieHelper.storeCookie(page);
  console.log(storeCookieFunc);

  /* Negative reaction. */
  const flowParam = 'negative';
  try {
    await page.goto(postUrl, { waitUntil: 'networkidle2' });
    comments = await commentServices.loadComment(flowParam);

    const likePostFunc = await postServices.likePost(page, flowParam);
    console.log(likePostFunc);
    const commentPostFunc = await postServices.commentPost(
      page,
      comments[Math.floor(Math.random() * comments.length)]
    );
    console.log(commentPostFunc);
    const reportPostFunc = await postServices.reportPost(page);
    console.log(reportPostFunc);
    const accountFromPostFunc = await postServices.accountFromPost(page);
    console.log(accountFromPostFunc);
    const accountFollowFunc = await accountServices.followAccount(
      page,
      flowParam
    );
    console.log(accountFollowFunc);
    const reportAccountFunc = await accountServices.reportAccount(page);
    console.log(reportAccountFunc);
  } catch (error) {
    console.log(error);
  }

  await browser.close();
}

negative(false);
