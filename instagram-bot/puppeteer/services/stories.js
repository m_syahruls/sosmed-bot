const { storiesXpathList } = require('../constant');

const reportStories = async (page) => {
  return new Promise(async (resolve, reject) => {
    try {
      await page.waitForSelector(storiesXpathList.viewButton);
      await page.click(storiesXpathList.viewButton);
      await page.waitForSelector(storiesXpathList.moreButton);
      await page.click(storiesXpathList.moreButton);
      await page.waitForSelector(storiesXpathList.reportButton);
      await page.click(storiesXpathList.reportButton);
      await page.waitForSelector(storiesXpathList.reportItem);
      await page.click(storiesXpathList.reportItem);
      await page.waitForSelector(storiesXpathList.submitButton);
      await page.click(storiesXpathList.submitButton);
      await page.waitForSelector(storiesXpathList.closeButton);
      await page.click(storiesXpathList.closeButton);

      resolve('reportStories: successfully');
    } catch (error) {
      reject('reportStories error: ' + error);
    }
    await page.waitForTimeout(1000);
  });
};

module.exports = {
  reportStories,
};
