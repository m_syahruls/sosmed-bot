const { postXpathList } = require('../constant');

const likePost = async (page, param) => {
  return new Promise(async (resolve, reject) => {
    try {
      await page.waitForSelector(postXpathList.likeButton);
      try {
        await page.waitForSelector(postXpathList.likeButtonDeactivate, {
          timeout: 100,
        });
        if (param == 'positive') {
          await page.click(postXpathList.likeButton);
          resolve('likePost: successfully');
        }
      } catch (error) {
        if (param == 'negative') {
          await page.click(postXpathList.likeButton);
          resolve('unlikePost: successfully');
        }
      }

      resolve('ignorePost: successfully');
    } catch (error) {
      reject('likePost error: ' + error);
    }
    await page.waitForTimeout(1000);
  });
};

const commentPost = async (page, postComment) => {
  return new Promise(async (resolve, reject) => {
    try {
      await page.waitForSelector(postXpathList.commentButton);
      await page.click(postXpathList.commentButton);
      await page.keyboard.type(postComment);
      await page.keyboard.press('Enter');
      resolve('commentPost: successfully');
    } catch (error) {
      reject('commentPost error: ' + error);
    }
    await page.waitForTimeout(1000);
  });
};

const accountFromPost = async (page) => {
  return new Promise(async (resolve, reject) => {
    try {
      await page.waitForSelector(postXpathList.usernameLink);
      const hrefUrl = await page.$eval(postXpathList.usernameLink, (anchor) =>
        anchor.getAttribute('href')
      );
      await page.goto(`https://instagram.com${hrefUrl}`, {
        waitUntil: 'networkidle2',
      });
      resolve('accountFromPost: successfully');
    } catch (error) {
      reject('accountFromPost error: ' + error);
    }
    await page.waitForTimeout(1000);
  });
};

const reportPost = async (page) => {
  return new Promise(async (resolve, reject) => {
    try {
      await page.waitForSelector(postXpathList.moreButton);
      await page.click(postXpathList.moreButton);
      await page.waitForSelector(postXpathList.reportButton);
      await page.click(postXpathList.reportButton);
      await page.waitForSelector(postXpathList.reportItem);
      await page.click(postXpathList.reportItem);
      await page.waitForSelector(postXpathList.submitButton);
      await page.click(postXpathList.submitButton);
      await page.waitForSelector(postXpathList.closeButton);
      await page.click(postXpathList.closeButton);
      await page.waitForSelector(postXpathList.usernameLink);
      await page.click(postXpathList.usernameLink);

      resolve('reportPost: successfully');
    } catch (error) {
      reject('reportPost error: ' + error);
    }
    await page.waitForTimeout(1000);
  });
};

const sharePostFacebook = async (page) => {
  return new Promise(async (resolve, reject) => {
    try {
      await page.waitForSelector(postXpathList.moreButton);
      await page.click(postXpathList.moreButton);
      await page.waitForSelector(postXpathList.shareButton);
      await page.click(postXpathList.shareButton);
      await page.waitForSelector(postXpathList.shareFacebook);
      await page.click(postXpathList.shareFacebook);

      resolve('sharePostFacebook: successfully');
    } catch (error) {
      reject('sharePostFacebook error: ' + error);
    }
    await page.waitForTimeout(1000);
  });
};

const sharePostTwitter = async (page) => {
  return new Promise(async (resolve, reject) => {
    try {
      await page.waitForSelector(postXpathList.moreButton);
      await page.click(postXpathList.moreButton);
      await page.waitForSelector(postXpathList.shareButton);
      await page.click(postXpathList.shareButton);
      await page.waitForSelector(postXpathList.shareTwitter);
      await page.click(postXpathList.shareTwitter);

      resolve('sharePostTwitter: successfully');
    } catch (error) {
      reject('sharePostTwitter error: ' + error);
    }
    await page.waitForTimeout(1000);
  });
};

const sharePostLink = async (page) => {
  return new Promise(async (resolve, reject) => {
    try {
      await page.waitForSelector(postXpathList.moreButton);
      await page.click(postXpathList.moreButton);
      await page.waitForSelector(postXpathList.shareButton);
      await page.click(postXpathList.shareButton);
      await page.waitForSelector(postXpathList.shareLink);
      await page.click(postXpathList.shareLink);

      resolve('sharePostCopyLink: successfully');
    } catch (error) {
      reject('sharePostCopyLink error: ' + error);
    }
    await page.waitForTimeout(1000);
  });
};

const createPost = async (page, captionPost) => {
  return new Promise(async (resolve, reject) => {
    try {
      await page.waitForSelector(postXpathList.createButton);
      await page.click(postXpathList.createButton);

      await page.waitForSelector('input[type=file]');
      const input = await page.$('input[type=file]');
      await input.uploadFile('./storage/sample/171-800x800.jpg');

      await page.waitForSelector(postXpathList.nextCreateButton);
      await page.click(postXpathList.nextCreateButton);
      await page.waitForTimeout(500);
      await page.click(postXpathList.nextCreateButton);

      await page.waitForSelector(postXpathList.captionInput);
      await page.click(postXpathList.captionInput);
      await page.keyboard.type(captionPost);
      await page.click(postXpathList.nextCreateButton);

      await page.waitForSelector(postXpathList.sharedAlert);

      resolve('createPost: successfully');
    } catch (error) {
      reject('createPost error: ' + error);
    }
    await page.waitForTimeout(1000);
  });
};

module.exports = {
  likePost,
  commentPost,
  accountFromPost,
  reportPost,
  sharePostFacebook,
  sharePostTwitter,
  sharePostLink,
  createPost,
};
