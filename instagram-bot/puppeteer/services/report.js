const { reportByHarrashmentPageXpathList } = require('../constant');
const reportUrl = 'https://help.instagram.com/contact/188391886430254';

const {
  email,
  postLink,
  reelLink,
  usernameTarget,
  descriptionMessage,
  country,
} = {
  email: 'mipmip@yopmail.com',
  postLink: 'https://www.instagram.com/p/CqBDf3tLVc_/',
  reelLink: 'https://www.instagram.com/reel/CqLXD30jdF3/',
  usernameTarget: 'instagram',
  descriptionMessage: 'misinformation to threating some public figure',
  country: 'Indonesia',
};

const reportByHarrashmentPage = async (page, data) => {
  return new Promise(async (resolve, reject) => {
    try {
      await page.goto(reportUrl, { waitUntil: 'networkidle2' });
      await page.waitForSelector(reportByHarrashmentPageXpathList.reportRadio1);
      await page.click(reportByHarrashmentPageXpathList.reportRadio1);
      await page.click(reportByHarrashmentPageXpathList.reportRadio2);
      await page.click(reportByHarrashmentPageXpathList.reportRadio3);
      await page.type(reportByHarrashmentPageXpathList.reportInput1, email);
      await page.type(reportByHarrashmentPageXpathList.reportInput2, postLink);
      await page.type(reportByHarrashmentPageXpathList.reportInput3, reelLink);
      await page.type(
        reportByHarrashmentPageXpathList.reportInput4,
        usernameTarget
      );
      await page.type(reportByHarrashmentPageXpathList.reportInput5, country);
      await page.type(
        reportByHarrashmentPageXpathList.reportTextarea1,
        descriptionMessage
      );
      await page.click(reportByHarrashmentPageXpathList.submitButton);
      resolve('reportByHarrashmentPage: successfully');
    } catch (error) {
      reject('reportByHarrashmentPage error: ' + error);
    }
    await page.waitForTimeout(1000);
  });
};

module.exports = {
  reportByHarrashmentPage,
};
