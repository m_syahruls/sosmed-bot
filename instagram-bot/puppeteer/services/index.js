module.exports.reportServices = require('./report');
module.exports.loginServices = require('./login');
module.exports.postServices = require('./post');
module.exports.commentServices = require('./comment');
module.exports.internetThrottleServices = require('./internetThrottle');
module.exports.accountServices = require('./account');
module.exports.storiesServices = require('./stories');
