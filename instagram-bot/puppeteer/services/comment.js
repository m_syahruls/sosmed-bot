const fs = require('fs/promises');
const negativeCommentsPath = './storage/negativeComments.json';
const positiveCommentsPath = './storage/positiveComments.json';

const loadComment = async (param) => {
  return new Promise(async (resolve, reject) => {
    let path;
    param == 'positive'
      ? (path = positiveCommentsPath)
      : (path = negativeCommentsPath);

    try {
      const comments = await fs.readFile(path, {
        encoding: 'utf-8',
      });
      resolve(JSON.parse(comments));
    } catch (error) {
      reject('loadComment error: ' + error);
    }
  });
};

module.exports = {
  loadComment,
};
