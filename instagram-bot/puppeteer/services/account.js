const { accountXpathList } = require('../constant');

const followAccount = async (page, param) => {
  return new Promise(async (resolve, reject) => {
    try {
      await page.waitForSelector(accountXpathList.followButton);
      try {
        await page.waitForSelector(accountXpathList.followingButton, {
          timeout: 100,
        });
        if (param == 'negative') {
          await page.click(accountXpathList.followingButton);
          await page.waitForSelector(accountXpathList.unfollowItem);
          await page.click(accountXpathList.unfollowItem);
          resolve('unfollowAccount: successfully');
        }
      } catch (error) {
        if (param == 'positive') {
          await page.click(accountXpathList.followButton);
          resolve('followAccount: successfully');
        }
      }

      resolve('ignoreAccount: successfully');
    } catch (error) {
      reject('followAccount error: ' + error);
    }
    await page.waitForTimeout(1000);
  });
};

const reportAccount = async (page) => {
  return new Promise(async (resolve, reject) => {
    try {
      await page.waitForSelector(accountXpathList.moreButton);
      await page.click(accountXpathList.moreButton);
      await page.waitForSelector(accountXpathList.reportButton);
      await page.click(accountXpathList.reportButton);
      await page.waitForSelector(accountXpathList.reportItem1);
      await page.click(accountXpathList.reportItem1);
      await page.waitForSelector(accountXpathList.reportItem2);
      await page.click(accountXpathList.reportItem2);
      await page.waitForSelector(accountXpathList.reportItem3);
      await page.click(accountXpathList.reportItem3);
      await page.waitForSelector(accountXpathList.submitButton);
      await page.click(accountXpathList.submitButton);
      await page.waitForSelector(accountXpathList.closeButton);
      await page.click(accountXpathList.closeButton);

      resolve('reportAccount: successfully');
    } catch (error) {
      reject('reportAccount error: ' + error);
    }
    await page.waitForTimeout(1000);
  });
};

module.exports = {
  followAccount,
  reportAccount,
};
