const cookieHelper = require('../../../helper/cookie.js');
const { loginXpathList } = require('../constant');
const loginUrl = 'https://www.instagram.com/';

const login = async (page, email, password) => {
  return new Promise(async (resolve, reject) => {
    try {
      await page.goto(loginUrl, { waitUntil: 'networkidle2' });
      await page.waitForSelector(loginXpathList.usernameInput);

      await page.type(loginXpathList.usernameInput, email);
      await page.type(loginXpathList.passwordInput, password);
      await page.click(loginXpathList.submitButton);

      await page.waitForNavigation({ waitUntil: 'networkidle2' });

      const saveCookieFunc = await cookieHelper.saveCookie(page);
      console.log(saveCookieFunc);

      resolve('login: successfully');
    } catch (error) {
      reject('login error: ' + error);
    }
    await page.waitForTimeout(1000);
  });
};

const loginMulti = async (page, users) => {
  return new Promise(async (resolve, reject) => {
    try {
      await page.goto(loginUrl, { waitUntil: 'networkidle2' });
      await page.waitForSelector(loginXpathList.usernameInput);
      await page.waitForSelector(loginXpathList.passwordInput);

      /* used to handle looping multiple accounts */
      var isLogged = false;
      for (const [i, user] of users.entries()) {
        await page.click(loginXpathList.usernameInput, { clickCount: 3 });
        await page.keyboard.press('Backspace');
        await page.type(loginXpathList.usernameInput, user.email);

        await page.click(loginXpathList.passwordInput, { clickCount: 3 });
        await page.keyboard.press('Backspace');
        await page.type(loginXpathList.passwordInput, user.password);

        await page.waitForSelector(loginXpathList.submitButton);
        await page.click(loginXpathList.submitButton);

        /* used to check logged account with ajax response and home button */
        try {
          await page.waitForResponse((response) => response.status() === 403, {
            timeout: 1000,
          });
          console.log('loginMulti: account [' + i + '] failed');
          continue;
        } catch (error) {
          await page.waitForSelector(loginXpathList.homeButton);
          isLogged = true;
          break;
        }
      }

      if (isLogged == false) {
        throw 'all users invalid';
      }

      const saveCookieFunc = await cookieHelper.saveCookie(page);
      console.log(saveCookieFunc);

      resolve('loginMulti: successfully');
    } catch (error) {
      reject('loginMulti error: ' + error);
    }
    await page.waitForTimeout(1000);
  });
};

module.exports = {
  login,
  loginMulti,
};
