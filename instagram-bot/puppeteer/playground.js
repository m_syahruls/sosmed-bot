require('dotenv').config();
const puppeteer = require('puppeteer-extra');
const stealthPlugin = require('puppeteer-extra-plugin-stealth');
const {
  reportServices,
  loginServices,
  postServices,
  commentServices,
  accountServices,
  storiesServices,
  // internetThrottleServices,
} = require('./services');
puppeteer.use(stealthPlugin());
const cookieHelper = require('../../helper/cookie.js');

/* Get argument from the command line */
const args = process.argv.slice(2)[0];

/* might be input arguments from the users */
let comments;
const email = 'minztergundul@gmail.com';
const password = '7DosaBesar';
const postUrl = 'https://www.instagram.com/p/Cqafk-jj5HV/';
const storiesUrl =
  'https://www.instagram.com/stories/waroengpakmuh/3080491315868647623/';

const users = [
  {
    email: 'sminztergundul@gmail.com',
    password: '7DosaBesar',
  },
  {
    email: 'minztergundul@gmail.com',
    password: '7DosaBesar',
  },
  {
    email: 'sminztergundul@gmail.com',
    password: '7DosaBesar',
  },
];

const captionPost = `
  Assalamualaikum!

  Udah buka puasa di @waroengpakmuh belum nih?
  
  Buruan dateng ya karena lagi ada promo, Makan ber 4 mulai dari 90k aja loh, ada banyak paket yang bisa kalian pilih, yuk sambut ramadhan tahun ini bersama @waroengpakmuh
  
  Promo ini hanya berlaku untuk pembelian DINE IN. Banyak banyak terima kasih ya🙏🏼
  
  ( Periode promo 22 Maret - 21 April 2023 )
  
  Syarat & Ketentuan:
  - HANYA BERLAKU DINE IN
  - Khusus promo ini Take Away akan di kenakan charge Rp 12.000
  - Ada perbedaan harga untuk luar jawa
  
  🕘 10.00 - 21.00
  🛵 Available on DINE IN
  
  #banyakbanyakterimakasih
  #resepkeluargajaidi
  #nikmatgroup
  `;
/* end of might be input arguments from the users */

async function checkAuth(page, email, password) {
  /* It's checking if there is a cookie. */
  const isAuth = await cookieHelper.checkCookie(page);
  if (!isAuth) {
    const loginFunc = await loginServices.login(page, email, password);
    console.log(loginFunc);
  }
  const storeCookieFunc = await cookieHelper.storeCookie(page);
  console.log(storeCookieFunc);
}

async function playground(paramHeadless) {
  const browser = await puppeteer.launch({
    headless: paramHeadless,
    args: [
      '--no-sandbox',
      '--disable-gpu',
      '--enable-webgl',
      '--start-maximized',
    ],
  });
  const page = await browser.newPage();

  /* Throttling the internet connection, 
  change the speed manually inside internetThrottle function. */
  // await internetThrottleServices.internetThrottle(page);

  switch (args) {
    case 'login':
      try {
        await loginServices.login(page, email, password);
      } catch (error) {
        console.log(error);
      }
      break;

    case 'login-multi':
      try {
        // await loginServices.loginMulti(page, users);
        const loginMultiFunc = await loginServices.loginMulti(page, users);
        console.log(loginMultiFunc);
      } catch (error) {
        console.log(error);
      }
      break;

    /* Negative reaction. */
    case 'negative-reaction':
      await checkAuth(page, email, password);
      var flowParam = 'negative';
      try {
        await page.goto(postUrl, { waitUntil: 'networkidle2' });
        comments = await commentServices.loadComment(flowParam);

        const likePostFunc = await postServices.likePost(page, flowParam);
        console.log(likePostFunc);
        const commentPostFunc = await postServices.commentPost(
          page,
          comments[Math.floor(Math.random() * comments.length)]
        );
        console.log(commentPostFunc);
        const reportPostFunc = await postServices.reportPost(page);
        console.log(reportPostFunc);
        const accountFromPostFunc = await postServices.accountFromPost(page);
        console.log(accountFromPostFunc);
        const accountFollowFunc = await accountServices.followAccount(
          page,
          flowParam
        );
        console.log(accountFollowFunc);
        const reportAccountFunc = await accountServices.reportAccount(page);
        console.log(reportAccountFunc);
      } catch (error) {
        console.log(error);
      }
      break;

    /* Positive reaction. */
    case 'positive-reaction':
      await checkAuth(page, email, password);
      var flowParam = 'positive';
      try {
        await page.goto(postUrl, { waitUntil: 'networkidle2' });
        comments = await commentServices.loadComment(flowParam);

        const likePostFunc = await postServices.likePost(page, flowParam);
        console.log(likePostFunc);
        const commentPostFunc = await postServices.commentPost(
          page,
          comments[Math.floor(Math.random() * comments.length)]
        );
        console.log(commentPostFunc);
        const accountFromPostFunc = await postServices.accountFromPost(page);
        console.log(accountFromPostFunc);
        const accountFollowFunc = await accountServices.followAccount(
          page,
          flowParam
        );
        console.log(accountFollowFunc);
      } catch (error) {
        console.log(error);
      }
      break;

    case 'share-facebook':
      try {
        await page.goto(postUrl, { waitUntil: 'networkidle2' });
        await postServices.sharePostFacebook(page);
      } catch (error) {
        console.log(error);
      }
      break;

    case 'share-twitter':
      try {
        await page.goto(postUrl, { waitUntil: 'networkidle2' });
        await postServices.sharePostTwitter(page);
      } catch (error) {
        console.log(error);
      }
      break;

    case 'share-link':
      try {
        await page.goto(postUrl, { waitUntil: 'networkidle2' });
        await postServices.sharePostLink(page);
      } catch (error) {
        console.log(error);
      }
      break;

    case 'create-post':
      await checkAuth(page, email, password);
      try {
        await page.goto(postUrl, { waitUntil: 'networkidle2' });
        const createPostFunc = await postServices.createPost(page, captionPost);
        console.log(createPostFunc);
      } catch (error) {
        console.log(error);
      }
      break;

    case 'report-stories':
      await checkAuth(page, email, password);
      try {
        await page.goto(storiesUrl, { waitUntil: 'networkidle2' });
        const reportStoriesFunc = await storiesServices.reportStories(page);
        console.log(reportStoriesFunc);
      } catch (error) {
        console.log(error);
      }
      break;

    /* Report the profile. */
    default:
      try {
        const reportHarrashmentFunc =
          await reportServices.reportByHarrashmentPage(page);
        console.log(reportHarrashmentFunc);
      } catch (error) {
        console.log(error);
      }
      break;
  }

  await browser.close();
}

playground(false);
