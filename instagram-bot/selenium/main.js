const { Builder } = require('selenium-webdriver');
const { loginServices, reportServices, postServices } = require('./services');

require('dotenv').config();
require('chromedriver');
var chrome = require('selenium-webdriver/chrome');
var options = new chrome.Options();

const { debuggerAddress, email, password } = {
  debuggerAddress: process.env.DEBUGGER_ADDRESS,
  email: process.env.EMAIL,
  password: process.env.PASSWORD,
};

// options.addArguments('--headless');
// options.addArguments('--no-gpu');
options.addArguments('--disable-dev-shm-usage'); // overcome limited resource problems
options.addArguments('--disable-notifications'); // disable notifications like allow mic, camera, etc
options.addArguments('--disable-site-isolation-trials');
// options.debuggerAddress(debuggerAddress); // to use existing browser
options.add;

main();

async function main() {
  try {
    let driver = await new Builder()
      .forBrowser('chrome')
      .setChromeOptions(options)
      .build();

    // // to get/generate debuggerAddress
    // await driver.get('https://instagram.com');
    const capabilities = await driver.getCapabilities();
    console.log(
      capabilities.map_.get('goog:chromeOptions').debuggerAddress.toString()
    );

    /* Reporting the harassment page. */
    // await reportServices.reportByHarrashmentPage(driver, 'mipmip@yopmail.com');

    /* Logging in to instagram. */
    // await loginServices.login(driver, email, password);
    // await loginServices.getSessionIdFromCookies(driver);

    /* Interact with the post. */
    // await driver.get('https://www.instagram.com/p/CqNqdeyjWOo/');
    // await postServices.likePost(driver);
    // await postServices.commentPost(driver);

    // await driver.close();
  } catch (error) {
    console.log('error: ', error);
    // await driver.close();
  }

  // It is always a safe practice to quit the browser after execution
  // await driver.quit();
}
