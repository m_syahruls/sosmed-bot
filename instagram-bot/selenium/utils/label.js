const { By, until } = require('selenium-webdriver');

const clickByLabelFor = async (driver, param) => {
  await driver
    .wait(until.elementLocated(By.xpath(`//label[@for="${param}"]`)), 5000)
    .then((el) => {
      el.click();
    });
};

module.exports = {
  clickByLabelFor,
};
