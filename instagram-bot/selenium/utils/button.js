const { By, until } = require('selenium-webdriver');

const submitButton = async (driver) => {
  await driver
    .wait(until.elementLocated(By.css('[type="submit"]')), 5000)
    .then((el) => {
      el.click();
    });
};

module.exports = {
  submitButton,
};
