const { By, until } = require('selenium-webdriver');

const clickByItemParamValue = async (driver, item, param, value) => {
  await driver
    .wait(
      until.elementLocated(By.xpath(`//${item}[${param}="${value}"]`)),
      5000
    )
    .then((el) => {
      el.click();
    });
};

const clickByXPath = async (driver, value) => {
  await driver
    .wait(until.elementLocated(By.xpath(`${value}`)), 5000)
    .then((el) => {
      el.click();
    });
};

const sendKeysByItemParamValue = async (
  driver,
  item,
  param,
  value,
  payload
) => {
  await driver
    .wait(
      until.elementLocated(By.xpath(`//${item}[${param}="${value}"]`)),
      5000
    )
    .then((el) => {
      el.sendKeys(payload);
    });
};

module.exports = {
  clickByItemParamValue,
  clickByXPath,
  sendKeysByItemParamValue,
};
