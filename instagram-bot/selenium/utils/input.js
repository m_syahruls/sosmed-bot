const { By, until } = require('selenium-webdriver');

const sendKeysByInputName = async (driver, param, payload) => {
  await driver
    .wait(until.elementLocated(By.xpath(`//input[@name="${param}"]`)), 5000)
    .then((el) => {
      el.sendKeys(payload);
    });
};

const sendKeysByTextareaName = async (driver, param, payload) => {
  await driver
    .wait(until.elementLocated(By.xpath(`//textarea[@name="${param}"]`)), 5000)
    .then((el) => {
      el.sendKeys(payload);
    });
};

module.exports = {
  sendKeysByInputName,
  sendKeysByTextareaName,
};
