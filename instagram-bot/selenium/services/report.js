const { buttonUtils, inputUtils, labelUtils } = require('../utils');

var { postLink, reelLink, usernameTarget, descriptionMessage, country } = {
  postLink: 'https://www.instagram.com/p/CqBDf3tLVc_/',
  reelLink: 'https://www.instagram.com/reel/CqLXD30jdF3/',
  usernameTarget: 'instagram',
  descriptionMessage: 'misinformation to threating some public figure',
  country: 'Indonesia',
};

async function reportByHarrashmentPage(driver, email) {
  await driver.get('https://help.instagram.com/contact/188391886430254');

  await labelUtils.clickByLabelFor(driver, '276492892513481.1');
  await labelUtils.clickByLabelFor(driver, '635963936475268.0');
  await labelUtils.clickByLabelFor(driver, '225201504335913.0');

  await inputUtils.sendKeysByInputName(driver, 'Field154118661891293', email);
  await inputUtils.sendKeysByInputName(
    driver,
    'Field1462983820598704',
    postLink
  );
  await inputUtils.sendKeysByInputName(
    driver,
    'Field1537886036712114',
    reelLink
  );
  await inputUtils.sendKeysByInputName(
    driver,
    'Field588598959732366',
    usernameTarget
  );
  await inputUtils.sendKeysByTextareaName(
    driver,
    'Field677805268950231',
    descriptionMessage
  );
  await inputUtils.sendKeysByInputName(driver, 'Field734854559869495', country);

  await buttonUtils.submitButton(driver);
}

module.exports = {
  reportByHarrashmentPage,
};
