const { seleniumUtils } = require('../utils');

var postLinks = [
  'https://www.instagram.com/p/CqNqdeyjWOo/',
  'https://www.instagram.com/p/CqBDf3tLVc_/',
];

var postComments = [
  'mantap',
  'apa atuh',
  'lorem ipsum dolor sit amet, consectetur',
];

async function likePost(driver) {
  // await driver.get(postLinks[0]);

  await seleniumUtils.clickByItemParamValue(driver, 'span', '@class', '_aamw');
}

// not stable yet
async function commentPost(driver) {
  // await driver.get(postLinks[0]);

  await seleniumUtils.clickByItemParamValue(driver, 'span', '@class', '_aamx');
  await seleniumUtils.sendKeysByItemParamValue(
    driver,
    'textarea',
    '@aria-label',
    'Add a comment…',
    postComments[0]
  );
  await seleniumUtils.clickByItemParamValue(driver, 'div', 'text()', 'Post');
}

module.exports = {
  likePost,
  commentPost,
};
