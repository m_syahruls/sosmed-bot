const { buttonUtils, inputUtils, seleniumUtils } = require('../utils');
var fs = require('fs');

async function login(driver, email, password) {
  await driver.get('https://www.instagram.com/');

  await inputUtils.sendKeysByInputName(driver, 'username', email);
  await inputUtils.sendKeysByInputName(driver, 'password', password);
  await buttonUtils.submitButton(driver);
}

async function getSessionIdFromCookies(driver) {
  const cookie = await driver.manage().getCookie('sessionid');
  console.log(cookie);

  fs.readFile('./storage/cookies.json', async (err, data) => {
    var json = JSON.parse(data);
    if (!json.sessionid.find((item) => item.id === json.sessionid.value))
      json.sessionid.push(cookie.value);

    fs.writeFileSync('./storage/cookies.json', JSON.stringify(json), (err) => {
      if (err) throw err;
    });

    if (err) throw err;
  });
}

module.exports = {
  login,
  getSessionIdFromCookies,
};
