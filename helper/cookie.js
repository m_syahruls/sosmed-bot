const fs = require('fs');
const { readFile, writeFile } = require('fs/promises');
const path = './storage/cookie.json';

const checkCookie = async (page) => {
  return new Promise(async (resolve, reject) => {
    try {
      fs.existsSync(path) ? resolve(true) : resolve(false);
    } catch (error) {
      reject('authChecker error: ' + error);
    }
  });
};

const storeCookie = async (page) => {
  return new Promise(async (resolve, reject) => {
    try {
      const cookie = await readFile(path, {
        encoding: 'utf-8',
      });
      const cookieParse = JSON.parse(cookie);
      await page.setCookie(...cookieParse);
      resolve('storeCookie: successfully');
    } catch (error) {
      reject('storeCookie error: ' + error);
    }
  });
};

const saveCookie = async (page) => {
  return new Promise(async (resolve, reject) => {
    try {
      const cookie = await page.cookies();
      await writeFile(path, JSON.stringify(cookie, null, 2));
      resolve('saveCookie: successfully');
    } catch (error) {
      reject('saveCookie error: ' + error);
    }
  });
};

module.exports = {
  checkCookie,
  storeCookie,
  saveCookie,
};
