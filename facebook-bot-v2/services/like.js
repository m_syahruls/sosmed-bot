const { xpath } = require("../constants");

const like = async (page) => {
  return new Promise(async (resolve, reject) => {
    try {
      await page.waitForSelector(xpath.likeButton);
      try {
        await page.click(xpath.likeButton);
      } catch (error) {}
      resolve("likePost: successfully");
    } catch (error) {
      reject("likePost error: " + error);
    }
    await page.waitForTimeout(1000);
  });
};

module.exports = {
  like,
};
