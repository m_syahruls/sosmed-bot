const fs = require("fs/promises");
const { xpath } = require("../constants");

const negativeCommentsPath = "./storage/negativeComments.json";
const positiveCommentsPath = "./storage/positiveComments.json";

const commentPost = async (page, postComment) => {
  return new Promise(async (resolve, reject) => {
    try {
      await page.waitForSelector(xpath.inputComment);
      await page.click(xpath.inputComment);
      await page.keyboard.type(postComment);
      await page.keyboard.press("Enter");
      resolve("commentPost: successfully");
    } catch (error) {
      reject("commentPost error: " + error);
    }
    await page.waitForTimeout(1000);
  });
};

const loadComment = async (param) => {
  return new Promise(async (resolve, reject) => {
    let path;
    param == "positive"
      ? (path = positiveCommentsPath)
      : (path = negativeCommentsPath);

    try {
      const comments = await fs.readFile(path, {
        encoding: "utf-8",
      });
      resolve(JSON.parse(comments));
    } catch (error) {
      reject("loadComment error: " + error);
    }
  });
};

module.exports = {
  loadComment,
  commentPost,
};
