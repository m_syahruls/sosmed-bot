const { xpath } = require("../constants");

require("dotenv").config();

const loginUrl = process.env.FACEBOOK_BASE_URL;

const login = async (page, email, password) => {
  return new Promise(async (resolve, reject) => {
    try {
      await page.goto(loginUrl, { waitUntil: "networkidle2" });
      await page.waitForTimeout(3000);
      await page.type(xpath.loginInputEmail, email);
      await page.type(xpath.loginInputPassword, password);
      await page.click(xpath.loginButton);
      await page.waitForNavigation({ waitUntil: "networkidle2" });
      resolve("login: successfully");
    } catch (error) {
      reject("login error: " + error);
    }
    await page.waitForTimeout(1000);
  });
};

module.exports = {
  login,
};
