const { xpath } = require("../constants");

const reportByPost = async (page) => {
  return new Promise(async (resolve, reject) => {
    try {
      await page.waitForSelector(xpath.postContextMenuButton);
      await page.click(xpath.postContextMenuButton);
      await page.waitForSelector(xpath.postReportButton);
      await page.click(xpath.postReportButton);
      await page.waitForSelector(xpath.postReportHoax);
      await page.click(xpath.postReportHoax);
      await page.waitForSelector(xpath.postReportSocialIssue);
      await page.click(xpath.postReportSocialIssue);
      await page.waitForSelector(xpath.postReportSendButton);
      await page.click(xpath.postReportSendButton);
      resolve("reportByPost: successfully");
    } catch (error) {
      reject("reportByPost error: " + error);
    }
    await page.waitForTimeout(1000);
  });
};

module.exports = {
  reportByPost,
};
