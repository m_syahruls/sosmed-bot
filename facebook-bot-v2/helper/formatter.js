const formatter = {
  urlFromEmbedElement: (frameElement) =>
    decodeURIComponent(
      `https://${frameElement?.split("www")?.[2].split("&")?.[0]}`.replace(
        ".facebook",
        "facebook"
      )
    ),
};

module.exports = {
  formatter,
};
