require("dotenv").config();

const puppeteer = require("puppeteer-extra");
const stealthPlugin = require("puppeteer-extra-plugin-stealth");
const { likeServices, loginServices, commentServices } = require("./services");
puppeteer.use(stealthPlugin());
const cookieHelper = require("../helper/cookie");
const { formatter } = require("./helper/formatter");

positive();

async function positive() {
  const browser = await puppeteer.launch({
    headless: false,
    args: [
      "--no-sandbox",
      "--disable-gpu",
      "--enable-webgl",
      "--window-size=1920,1080",
    ],
  });
  const page = await browser.newPage();

  let comments;
  const email = process.env.EMAIL;
  const password = process.env.PASSWORD;
  const postUrl = formatter.urlFromEmbedElement(process.env.FACEBOOK_POST_URL);

  /* It's checking if there is a cookie. */
  const isAuth = await cookieHelper.checkCookie(page);
  console.log(isAuth, "login");
  if (!isAuth) {
    const loginFunc = await loginServices.login(page, email, password);
    console.log(loginFunc);
    const saveCookieFunc = await cookieHelper.saveCookie(page);
    console.log(saveCookieFunc);
  }
  const storeCookieFunc = await cookieHelper.storeCookie(page);
  console.log(storeCookieFunc);

  /* Positive reaction. */
  try {
    await page.goto(postUrl, { waitUntil: "networkidle2" });
    comments = await commentServices.loadComment("positive");
    const likePostFunc = await likeServices.like(page, postUrl);
    console.log(likePostFunc);
    const commentPostFunc = await commentServices.commentPost(
      page,
      comments[Math.floor(Math.random() * comments.length)]
    );
    console.log(commentPostFunc);
  } catch (error) {
    console.log(error);
  }

  await browser.close();
}
