require("dotenv").config();
const puppeteer = require("puppeteer-extra");
const stealthPlugin = require("puppeteer-extra-plugin-stealth");
const {
  loginServices,
  commentServices,
  reportServices,
} = require("./services");
puppeteer.use(stealthPlugin());
const cookieHelper = require("../helper/cookie.js");
const { formatter } = require("./helper/formatter");

negative();

async function negative() {
  const browser = await puppeteer.launch({
    headless: false,
    args: [
      "--no-sandbox",
      "--disable-gpu",
      "--enable-webgl",
      "--window-size=1920,1080",
    ],
  });
  const page = await browser.newPage();

  let comments;
  const email = process.env.EMAIL;
  const password = process.env.PASSWORD;
  const postUrl = formatter.urlFromEmbedElement(process.env.FACEBOOK_POST_URL);

  /* It's checking if there is a cookie. */
  const isAuth = await cookieHelper.checkCookie(page);
  if (!isAuth) {
    const loginFunc = await loginServices.login(page, email, password);
    console.log(loginFunc);
    const saveCookieFunc = await cookieHelper.saveCookie(page);
    console.log(saveCookieFunc);
  }
  const storeCookieFunc = await cookieHelper.storeCookie(page);
  console.log(storeCookieFunc);

  /* Negative reaction. */
  try {
    await page.goto(postUrl, { waitUntil: "networkidle2" });
    comments = await commentServices.loadComment("negative");

    const commentPostFunc = await commentServices.commentPost(
      page,
      comments[Math.floor(Math.random() * comments.length)]
    );
    console.log(commentPostFunc);
    const reportByPostFunc = await reportServices.reportByPost(page);
    console.log(reportByPostFunc);
  } catch (error) {
    console.log(error);
  }

  await browser.close();
}
